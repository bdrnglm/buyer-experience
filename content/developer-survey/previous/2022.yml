---
title: GitLab DevSecOps Survey Results 2022
og_title: GitLab DevSecOps Survey Results 2022
description: "With over 5000 respondents, the GitLab 2022 DevSecOps survey provides valuable strategic insight into the latest team structures and practices. Learn more here!"
twitter_description: "With over 5000 respondents, the GitLab 2022 DevSecOps survey provides valuable strategic insight into the latest team structures and practices. Learn more here!"
og_description: "With over 5000 respondents, the GitLab 2022 DevSecOps survey provides valuable strategic insight into the latest team structures and practices. Learn more here!"
og_image: /nuxt-images/developer-survey/2022-devsecops-survey-landing-page-opengraph.jpeg
twitter_image: /nuxt-images/developer-survey/2022-devsecops-survey-landing-page-opengraph.jpeg
side_navigation:
  links:
    - title: Overview
      href: "#overview"
    - title: Developers
      href: "#developers"
    - title: Security
      href: "#security"
    - title: Operations
      href: "#operations"
  cta:
    text: Read the survey
    href: "#download"
    icon:
      name: docs
      color: "#EEE7FD"
      color_dark: "#000000"
hero:
  note:
    -  The GitLab 2022
    -  Global DevSecOps Survey
  note_animation_duration: .8s
  title: Thriving in an insecure world
  title_animation_duration: .9s
  aos_animation: fade-down
  aos_duration: 800
  gradient: true
  display_size: true
  img_animation: zoom-out-left
  img_animation_duration: 1600
  primary_btn:
    text: Read the full survey
    url: "#download"
    data_ga_name: read the full survey
    data_ga_location: hero
    button_animation_duration: 1s
    icon:
      name: docs
      variant: marketing
  image:
    image_url: /nuxt-images/developer-survey/hero-2022-survey.svg
    image_url_mobile: /nuxt-images/developer-survey/hero-2022-survey.svg
    alt: "Image: gitLab for The GitLab 2022 Global DevSecOps Survey"
    small: true
  parallax: true
intro:
  copy:
    variant: light
    groups:
      - blocks:
          - text: In May 2022, over 5,000 DevOps professionals shared details about their teams and practices. Despite a challenging business environment, strong momentum continued in automation, release cadences, and cutting-edge technology adoption.
          - text: Secure software development is now an imperative for DevOps teams around the world. It’s the number one reason for  – and benefit of – DevOps platform usage.
      - blocks:
          - text: How will DevOps pros navigate the future? They told us a stronger reliance on soft skills such as communication and collaboration, and an advanced understanding of technologies, including AI/ML.
          - text: Read on for our snapshot of DevOps in 2022.
            arrow_icon: true
  benefit_hero:
    variant: light
    image:
      url: /nuxt-images/developer-survey/overview-2022-survey.svg
      alt: line graph
    percent: 70
    description: of DevOps teams release code continuously, once a day, or every few days, up 11% from 2021.
  benefits:
    variant: light
    benefits:
      - name: Automated testing is growing
        description: 47% of teams report their testing is fully automated today, up from 25% last year.
        icon:
          name: build
          alt: build icon
      - name: New technologies and methodologies
        description: 62% of survey takers are practicing ModelOps, while 51% use AI/ML to check (not test) code.
        icon:
          name: new-technologies
          alt: new technologies icon
  graph:
    variant: light
    blocks:
      - header: DevOps = automation
        graphs:
          - name: Fully automated in 2022
            value: 24
          - name: in 2021
            value: 19
            opacity: 0.6
          - name: in 2020
            value: 8
            opacity: 0.3
      - header: What does modern DevOps look like in 2022?
        graphs:
          - name: DevOps platforms in use
            value: 44
          - name: Teams practice DevSecOps
            value: 42
          - name: CI/CD is onboard
            value: 35
          - name: Observability/monitoring tools are in place
            value: 30
          - name: AI/ML is powering code review, software test and more
            value: 24
        footer: Respondents could choose all that apply
  highlight:
    variant: light
    question: Why use a DevOps platform?
    answers:
      - Security
      - Cost and time savings
      - Improved DevOps
      - Easier automation
      - Improved monitoring
      - Improved observability
      - Better metrics
developers:
  header:
    header: Developers
    description: As we’ve seen over the last three years, devs are taking on more ops responsibilities, as well as more ownership of security.
  benefit_hero:
    image:
      url: /nuxt-images/developer-survey/dev-2022-survey.svg
      alt: line graph
    percent: 35
    description: of devs are releasing code twice as fast, and 15% are releasing code between three and five times faster.
    footnote: All told, almost 60% acknowledged code is moving into production at a much faster clip.
  benefits:
    benefits:
      - name: Why the faster releases?
        description: We asked devs "what’s changed" and a majority said use of a DevOps platform, followed by automated testing, SCM, planning tools, and observability.
        icon:
          name: release
          alt: release icon
      - name: What do devs want more of?
        description: More code reviews, automated testing, and planning.
        icon:
          name: magnifying-glass-code
          alt: magnifying glass icon
      - name: If releases are delayed...
        description: devs blame code development, code review, security analysis, test data management, and, of course, testing.
        icon:
          name: delay
          alt: delay icon
      - name: Roles are changing
        description: Fully 38% of devs said they instrument the code they’ve written for production monitoring (up from 26% in 2021 and just 18% in 2020) and 38% monitor and respond to the infrastructure their apps are running on (up 25% from last year).
        icon:
          name: roles
          alt: roles icon
      - name: It’s a tough world
        description: Developers acknowledge that Covid-19, hiring, security threats, culture changes, and complex tech learning curves added more real-world difficulties to their roles than ever before.
        icon:
          name: world
          alt: world icon
      - name: Less is more
        description: Automation has lightened the dev load and eased the burden for manual testing, code review, opening tickets, and more.
        icon:
          name: digital-transformation
          alt: digital transformation icon
  graph:
    blocks:
      - header: Of time and toolchains
        graphs:
          - name: Devs who spend between one-quarter and one-half of their time on toolchain maintenance/integration
            value: ~40
        footer: More than double the 2021 percentage
      - custom_height: 40
        graphs:
          - name: Devs who devote at least half their time and as much as all of their time on toolchain integration and maintenance
            value: 33
  quotes:
    quotes:
      - text: We have a development capacity challenge, a recruiting challenge, and a knowledge-sharing challenge.
        author: Developer respondent
      - text: 4G, 5G, AI, Metaverse, virtual space - developers have to support all of this.
        author: Developer respondent
      - text: (Thanks to DevOps) we are no longer writing messy code and ignoring code quality.
        author: Developer respondent
      - text: Cyber security attacks are the biggest challenge facing us today.
        author: Developer respondent
security:
  header:
    variant: light
    header: Security
    description: Security pros are also seeing their roles change, particularly when it comes to getting “hands on” with dev teams to get things done.
  benefit_hero:
    variant: light
    image:
      url: /nuxt-images/developer-survey/sec-2022-survey.svg
      alt: line graph
    percent: 71
    description: rated their organization’s security efforts as either “good” or “excellent.”
  benefits:
    variant: light
    benefits:
      - name: The great shift left continues
        description: 57% of sec team members said their orgs have either shifted security left or are planning to this year. One-third of teams, though, aren’t thinking about a shift left until at least two years from now.
        icon:
          name: shift-left-alt
          alt: shift-left icon
      - name: Who owns sec?
        description: As we’ve seen in previous surveys, this is still an area in need of clarity. 43% of sec team members admitted to full ownership of security (a 12% jump from last year), but a resounding majority (53%) said everyone was responsible, a 25% increase from 2021.
        icon:
          name: security-owner
          alt: security owner icon
      - name: Not as optimistic
        description: Concern about security has never been higher, so perhaps it’s not surprising 43% of sec pros feel “somewhat” or “very” unprepared for the future.
        icon:
          name: security-warning
          alt: security warning icon
      - name: In the future...
        description: a majority of sec pros think AI/ML skills will help their careers the most, followed by communication and collaboration (33% each).
        icon:
          name: future-roles
          alt: future roles icon
      - name: All in a day’s work
        description: 35% are more involved in daily tasks/more hands on, an 11-point jump from last year.
        icon:
          name: calendar-check
          alt: calendar check icon
  graph:
    variant: light
    blocks:
      - header: Security scanning is increasing…
        description: Across the board, devs report greater usage of scanning…
        graphs:
          - name: run SAST scans (a dramatic jump from last year, which was less than 40%)
            value: 53
          - name: employ dynamic application security testing (DAST) scans (up 11 points from last year)
            value: 55
          - name: scan containers today (up 10% from 2021)
            value: ~60
          - name: perform dependency scans
            value: 56
          - name: ensure license compliance checks
            value: 61
      - header: …but easy access to data lags
        description: The majority of dev teams still aren’t getting scan data in their workflows.
        graphs:
          - name: have SAST lite scanners in a web IDE
            value: 30
          - name: pull scan results into a web pipeline report for devs
            value: 29
          - name: make DAST, container and dependency scans easily available to devs
            value: 29
operations:
  header:
    header: Operations
    description: No one wears more hats on a DevOps team than an ops pro, and their roles continue to shift dramatically.
  benefit_hero:
    image:
      url: /nuxt-images/developer-survey/ops-2022-survey.svg
      alt: line graph
    percent: 44
    description: of ops teams are “mostly” automated and almost one-quarter of ops teams report full automation, both big jumps from 2021.
  benefits:
    benefits:
      - name: Compliance and audits FTW
        description: Most ops pros spend between one-quarter and half their time on audit and compliance, a 15% increase from 2021. And almost 25% of ops pros spend between half and three-quarters of their time dealing with audit and compliance.
        icon:
          name: contracts
          alt: contracts icon
      - name: The DevSecOps gets real
        description: Just over 76% of ops teams agree at some level that devs are able to receive and address security issues during the development process (that’s a 10% jump from last year).
        icon:
          name: cog-code
          alt: cog code icon
      - name: Developer enablement
        description: ~77% of ops pros said their devs are able to provision testing environments, which is an 8% increase from last year.
        icon:
          name: dev-enablement
          alt: developer enablement icon
  graph:
    blocks:
      - header: Too much information!
        description: Operations pros…
        graphs:
          - name: acknowledge the data exists but accessing/management is difficult
            value: 39
          - name: are "overwhelmed" by amount/scope of data
            value: 27
      - custom_height: 68
        graphs:
          - name: don't know what's available or org doesn't track what they need
            value: 14
          - name: have all data necessary and it's easy to access
            value: 18
  highlight:
    question: What changes has DevOps brought to the ops role?
    answers:
      - Managing the cloud
      - Overseeing all compliance
      - Managing infrastructure
      - Responsibility for automation
      - Maintaining the toolchain
      - '"A DevOps coach"'
      - '"A platform engineer"'
download:
  form_block:
    icon:
      name: docs
      variant: marketing
      alt: Docs Icon
    header: Download the full DevSecOps Survey
    description: Dig deeper into the data. Get access to the entire survey now for a complete picture of the state of DevOps in 2022.
    form:
      form_id: 1002
      datalayer: events
  links_block:
    header: Dive into our previous reports
    links:
      - text: 2021
        url: https://about.gitlab.com/developer-survey/previous/2021/
      - text: 2020
        url: https://about.gitlab.com/developer-survey/previous/2020/
      - text: 2019
        url: https://about.gitlab.com/developer-survey/previous/2019/
      - text: 2018
        url: https://about.gitlab.com/developer-survey/previous/2018/