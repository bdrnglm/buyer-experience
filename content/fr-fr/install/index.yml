---
  title: Télécharger et installer GitLab
  description: Téléchargez, installez et maintenez votre propre instance de GitLab avec divers paquets d'installation et téléchargements pour Linux, Kubernetes, Docker, Google Cloud et plus encore.
  side_menu:
    anchors:
      text: "Sur cette page"
      data:
      - text: Paquet Linux officiel
        href: "#official-linux-package"
        data_ga_name: official linux package
        data_ga_location: side menu
      - text: Déploiements Kubernetes
        href: "#kubernetes-deployments"
        data_ga_name: kubernetes deployments
        data_ga_location: side menu
      - text: Nuage pris en charge
        href: "#supported-cloud"
        data_ga_name: supported cloud
        data_ga_location: side menu
      - text: Autres méthodes officielles
        href: "#other-official-methods"
        data_ga_name: other official methods
        data_ga_location: side menu
      - text: Contribution communautaire
        href: "#community-contributed"
        data_ga_name: community contributed
        data_ga_location: side menu
      - text: Déjà installé ?
        href: "#already-installed"
        data_ga_name: already installed
        data_ga_location: side menu
    hyperlinks:
      text: "Plus d'informations sur ce sujet"
      data:
        - text: "Obtenir un essai gratuit"
          href: "https://gitlab.com/-/trial_registrations/new?glm_source=about.gitlab.com&glm_content=default-saas-trial"
          data_ga_name: "free trial"
          data_ga_location: "side-navigation"
        - text: "Installer l'autogestion"
          href: "/fr-fr/free-trial/"
          data_ga_name: "self managed trial"
          data_ga_location: "side-navigation"
        - text: "Démarrer avec SaaS"
          href: "https://gitlab.com/-/trial_registrations/new?glm_source=about.gitlab.com&glm_content=default-saas-trial"
          data_ga_name: "saas get started"
          data_ga_location: "side-navigation"
        - text: "Achat sur les places de marché"
          href: "https://page.gitlab.com/cloud-partner-marketplaces.html"
          data_ga_name: "partner marketplace"
          data_ga_location: "side-navigation"
  header:
    title: Installer GitLab autogéré
    subtitle: Essayez GitLab dès aujourd'hui. Téléchargez, installez et maintenez votre propre instance de GitLab.
    text: |
      [Essai](https://gitlab.com/-/trial_registrations/new?glm_source=about.gitlab.com&glm_content=default-saas-trial){data-ga-name="free trial" data-ga-location="header"} - commencez votre essai gratuit dès aujourd'hui

      [Autogestion](/fr-fr/free-trial/){data-ga-name="self managed trial" data-ga-location="header"} - installez sur votre propre infrastructure

      [SaaS](https://gitlab.com/-/trial_registrations/new?glm_source=about.gitlab.com&glm_content=default-saas-trial){data-ga-name="saas get started" data-ga-location="header"}  - démarrez avec notre offre SaaS

      [Marketplace](https://page.gitlab.com/cloud-partner-marketplaces.html){data-ga-name="partner marketplace" data-ga-location="header"}  - acheter en toute transparence en utilisant la place de marché en nuage de votre choix
  linux:
    title: Paquet Linux officiel
    subtitle: Méthode d'installation recommandée
    text: |
      C'est la méthode recommandée pour commencer. Les paquets Linux sont matures, évolutifs et sont utilisés aujourd'hui sur GitLab.com. Si vous avez besoin de plus de flexibilité et de résilience, nous vous recommandons de déployer GitLab comme décrit dans la [documentation de l'architecture de référence](https://docs.gitlab.com/ee/administration/reference_architectures/index.html){data-ga-name="reference architecture documentation" data-ga-location="linux installation"}

      L'installation Linux est plus rapide à installer, plus facile à mettre à jour et contient des caractéristiques qui améliorent la fiabilité et que l'on ne trouve pas dans d'autres méthodes. L'installation se fait via un paquetage unique (également connu sous le nom d'Omnibus) qui regroupe tous les différents services et outils nécessaires pour faire fonctionner GitLab. Il est recommandé d'avoir au moins 4 Go de RAM ([exigences minimales](https://docs.gitlab.com/ee/install/requirements.html){data-ga-name="installation requirements documentation" data-ga-location="linux installation"}).
    cards:
      - title: Ubuntu
        id: ubuntu
        subtext: 18.04 LTS, 20.04 LTS, 22.04 LTS
        icon:
          name: ubuntu
          variant: marketing
          alt: Ubuntu Icon
          hex_color: '#E95420'
        link_text: Voir les instructions d'installation +
        link_url: /fr-fr/install/#ubuntu
        data_ga_name: ubuntu installation documentation
        data_ga_location: linux installation
      - title: Debian
        id: debian
        subtext: 10, 11
        icon:
          name: debian
          variant: marketing
          alt: Debian Icon
          hex_color: '#000'
        link_text: Voir les instructions d'installation +
        link_url: /fr-fr/install/#debian
        data_ga_name: debian installation documentation
        data_ga_location: linux installation
      - title: AlmaLinux 8
        id: almalinux-8
        subtext: et RHEL, Oracle, Scientific
        icon:
          name: almalinux
          variant: marketing
          alt: Almalinux Icon
        link_text: Voir les instructions d'installation +
        link_url: /fr-fr/install/#almalinux-8
        data_ga_name: almalinux 8 installation documentation
        data_ga_location: linux installation
      - title: CentOS 7
        id: centos-7
        subtext: et RHEL, Oracle, Scientific
        icon:
          name: centos
          variant: marketing
          alt: CentOs Icon
        link_text: Voir les instructions d'installation +
        link_url: /fr-fr/install/#centos-7
        data_ga_name: centos 7 installation documentation
        data_ga_location: linux installation
      - title: OpenSUSE Leap
        id: opensuse-leap
        subtext: OpenSUSE Leap 15.4 et SUSE Linux Enterprise Server 12.2, 12.5
        icon:
          name: opensuse
          variant: marketing
          alt: OpenSuse Icon
        link_text: Voir les instructions d'installation +
        link_url: /fr-fr/install/#opensuse-leap
        data_ga_name: opensuse leap installation documentation
        data_ga_location: linux installation
      - title: Amazon Linux 2
        id: amazonlinux-2
        subtext:
        icon:
          name: aws
          variant: marketing
          alt: AWS Icon
        link_text: Voir les instructions d'installation +
        link_url: /fr-fr/install/#amazonlinux-2
        data_ga_name: amazonlinux 2 installation documentation
        data_ga_location: linux installation
      - title: Amazon Linux 2022
        id: amazonlinux-2022
        subtext:
        icon:
          name: aws
          variant: marketing
          alt: AWS Icon
        link_text: Voir les instructions d'installation +
        link_url: /fr-fr/install/#amazonlinux-2022
        data_ga_name: amazonlinux 2022 installation documentation
        data_ga_location: linux installation

      - title: Système d'exploitation Raspberry Pi
        id: raspberry-pi-os
        subtext: Bullseye et Buster (32 bit)
        icon:
          name: raspberry-pi
          variant: marketing
          alt: Raspberry Pi Icon
          hex_color: '#000'
        link_text: Voir les instructions d'installation +
        link_url: /fr-fr/install/#raspberry-pi-os
        data_ga_name: raspberry pi os installation documentation
        data_ga_location: linux installation
    dropdowns:
      - id: ubuntu
        tip: |
          Pour Ubuntu 20.04 et 22.04, des paquets `arm64` sont également disponibles et seront automatiquement utilisés sur cette plateforme lors de l'utilisation du dépôt GitLab pour l'installation.
        dependency_command: |
          ```
          sudo apt-get update
          sudo apt-get install -y curl openssh-server ca-certificates tzdata perl
          ```
        postfix_command: |
          ```
           sudo apt-get install -y postfix
          ```
        download_command: |
          ```
           curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash
          ```
        install_command: |
          ```
           sudo EXTERNAL_URL="https://gitlab.example.com" apt-get install gitlab-ee
          ```
        link_back: ubuntu
      - id: debian
        tip: |
          Pour Debian 10, les paquets `arm64` sont également disponibles et seront automatiquement utilisés sur cette plateforme lors de l'utilisation du dépôt GitLab pour l'installation.
        dependency_command: |
          ```
          sudo apt-get update
          sudo apt-get install -y curl openssh-server ca-certificates perl
          ```
        postfix_command: |
          ```
          sudo apt-get install -y postfix
          ```
        download_command: |
          ```
            curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash
          ```
        install_command: |
          ```
          sudo EXTERNAL_URL="https://gitlab.example.com" apt-get install gitlab-ee
          ```
        line_back: debian
      - id: almalinux-8
        tip: |
          Pour AlmaLinux et RedHat 8, des paquets `arm64` sont également disponibles et seront automatiquement utilisés sur cette plateforme lors de l'utilisation du dépôt GitLab pour l'installation.
        dependency_text: Sur AlmaLinux 8 (et RedHat 8), les commandes ci-dessous ouvriront également les accès HTTP, HTTPS et SSH dans le pare-feu du système. Cette étape est facultative, et vous pouvez la sauter si vous avez l'intention d'accéder à GitLab uniquement à partir de votre réseau local.
        dependency_command: |
          ```
          sudo dnf install -y curl policycoreutils openssh-server perl
          # Activez le daemon OpenSSH server s'il n'est pas activé : sudo systemctl status sshd
          sudo systemctl enable sshd
          sudo systemctl start sshd
          # Vérifiez si l'ouverture du firewall est nécessaire avec : sudo systemctl status firewalld
          sudo firewall-cmd --permanent --add-service=http
          sudo firewall-cmd --permanent --add-service=https
          sudo systemctl reload firewalld
          ```
        postfix_command: |
          ```
          sudo dnf install postfix
          sudo systemctl enable postfix
          sudo systemctl start postfix
          ```
        download_command: |
          ```
          curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.rpm.sh | sudo bash
          ```
        install_command: |
          ```
          sudo EXTERNAL_URL="https://gitlab.example.com" dnf install -y gitlab-ee
          ```
        line_back: almalinux-8
      - id: centos-7
        dependency_text: Sous CentOS 7 (et RedHat/Oracle/Linux scientifique 7), les commandes ci-dessous ouvriront également les accès HTTP, HTTPS et SSH dans le pare-feu du système. Cette étape est facultative, et vous pouvez la sauter si vous avez l'intention d'accéder à GitLab uniquement à partir de votre réseau local.
        dependency_command: |
          ```
          sudo yum install -y curl policycoreutils-python openssh-server perl
           Activer le daemon OpenSSH s'il n'est pas activé : sudo systemctl status sshd
          sudo systemctl enable sshd
          sudo systemctl start sshd
          # Vérifier si l'ouverture du firewall est nécessaire avec : sudo systemctl status firewalld
          sudo firewall-cmd --permanent --add-service=http
          sudo firewall-cmd --permanent --add-service=https
          sudo systemctl reload firewalld
          ```
        postfix_command: |
          ```
          sudo yum install postfix
          sudo systemctl enable postfix
          sudo systemctl start postfix
          ```
        download_command: |
          ```
          curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.rpm.sh | sudo bash
          ```
        install_command: |
          ```
          sudo EXTERNAL_URL="https://gitlab.example.com" yum install -y gitlab-ee
          ```
        line_back: centos-7
      - id: opensuse-leap
        tip: |
          Pour OpenSuse, des paquets `arm64` sont également disponibles et seront automatiquement utilisés sur cette plateforme lors de l'utilisation du dépôt GitLab pour l'installation.
        dependency_text: Sur OpenSUSE, les commandes ci-dessous ouvriront également les accès HTTP, HTTPS et SSH dans le pare-feu du système. Cette étape est facultative, et vous pouvez l'ignorer si vous avez l'intention d'accéder à GitLab uniquement à partir de votre réseau local.
        dependency_command: |
          ```
          sudo zypper install curl openssh perl
          # Activer le daemon OpenSSH s'il n'est pas activé :  sudo systemctl status sshd
          sudo systemctl enable sshd
          sudo systemctl start sshd
          # Vérifier si l'ouverture du firewall est nécessaire avec : sudo systemctl status firewalld
          sudo firewall-cmd --permanent --add-service=http
          sudo firewall-cmd --permanent --add-service=https
          sudo systemctl reload firewalld
          ```
        postfix_command: |
          ```
          sudo zypper install postfix
          sudo systemctl enable postfix
          sudo systemctl start postfix
          ```
        download_command: |
          ```curl -sS https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.rpm.sh | sudo bash```
        install_command: |
          ```sudo EXTERNAL_URL="https://gitlab.example.com" zypper install gitlab-ee```
        line_back: opensuse-leap

      - id: amazonlinux-2022
        tip: |
          Pour Amazon Linux 2022, des paquets `arm64` sont également disponibles et seront automatiquement utilisés sur cette plateforme lors de l'utilisation du dépôt GitLab pour l'installation.
        dependency_text: Sur Amazon Linux 2022, les commandes ci-dessous ouvriront également l'accès HTTP, HTTPS et SSH dans le pare-feu du système. Cette étape est facultative, et vous pouvez la sauter si vous avez l'intention d'accéder à GitLab uniquement à partir de votre réseau local.
        dependency_command: |
          ```
          sudo dnf install -y curl policycoreutils openssh-server perl
          # Activez le daemon OpenSSH server s'il n'est pas activé : sudo systemctl status sshd
          sudo systemctl enable sshd
          sudo systemctl start sshd
          # Vérifiez si l'ouverture du firewall est nécessaire avec : sudo systemctl status firewalld
          sudo firewall-cmd --permanent --add-service=http
          sudo firewall-cmd --permanent --add-service=https
          sudo systemctl reload firewalld
          ```
        postfix_command: |
          ```
          sudo dnf install postfix
          sudo systemctl enable postfix
          sudo systemctl start postfix
          ```
        download_command: |
          ```
          curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.rpm.sh | sudo bash
          ```
        install_command: |
          ```
          sudo EXTERNAL_URL="https://gitlab.example.com" dnf install -y gitlab-ee
          ```
        line_back: amazonlinux-2022

      - id: amazonlinux-2
        tip: |
          Pour Amazon Linux 2, les paquets `arm64` sont également disponibles et seront automatiquement utilisés sur cette plateforme lors de l'utilisation du dépôt GitLab pour l'installation.
        dependency_text: Sur Amazon Linux 2, les commandes ci-dessous ouvriront également les accès HTTP, HTTPS et SSH dans le pare-feu du système. Cette étape est facultative, et vous pouvez la sauter si vous avez l'intention d'accéder à GitLab uniquement à partir de votre réseau local.
        dependency_command: |
          ```
          sudo yum install -y curl policycoreutils-python openssh-server openssh-clients perl
          # Activez le daemon OpenSSH server s'il n'est pas activé : sudo systemctl status sshd
          sudo systemctl enable sshd
          sudo systemctl start sshd
          # Vérifiez si l'ouverture du firewall est nécessaire avec : sudo systemctl status firewalld
          sudo firewall-cmd --permanent --add-service=http
          sudo firewall-cmd --permanent --add-service=https
          sudo systemctl reload firewalld
          ```
        postfix_command: |
          ```
          sudo yum install postfix
          sudo systemctl enable postfix
          sudo systemctl start postfix
          ```
        download_command: |
          ```
          curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.rpm.sh | sudo bash
          ```
        install_command: |
          ```
          sudo EXTERNAL_URL="https://gitlab.example.com" yum install -y gitlab-ee
          ```
        line_back: amazonlinux-2
      - id: raspberry-pi-os
        tip: |
          Raspberry Pi 4 avec au moins 4 Go est recommandé. Seul le 32bit (armhf) est supporté pour le moment. Le 64 bits (`arm64`) est en route.
        dependency_command: |
          ```
          sudo apt-get install curl openssh-server ca-certificates apt-transport-https perl
          curl https://packages.gitlab.com/gpg.key | sudo tee /etc/apt/trusted.gpg.d/gitlab.asc
          ```
        postfix_command: |
          ```
          sudo apt-get install -y postfix
          ```
        download_command: |
          ```
          sudo curl -sS https://packages.gitlab.com/install/repositories/gitlab/raspberry-pi2/script.deb.sh | sudo bash
          ```
        install_command: |
          ```
          sudo EXTERNAL_URL="https://gitlab.example.com" apt-get install gitlab-ce
          ```
  kubernetes:
    title: Déploiements Kubernetes
    text: |
      Lors de l'installation de GitLab sur Kubernetes, il y a quelques compromis dont il faut être conscient :

        - L'administration et le dépannage nécessitent des connaissances sur Kubernetes
        - Cela peut être plus coûteux pour les petites installations. L'installation par défaut nécessite plus de ressources que le déploiement d'un paquet Linux sur un seul nœud, car la plupart des services sont déployés de manière redondante.
        - Il y a quelques limitations de fonctionnalités à [connaître.](https://docs.gitlab.com/charts/#limitations){data-ga-name="chart limitations" data-ga-location="kubernetes installation"}

      Utilisez cette méthode si votre infrastructure est construite sur Kubernetes et que vous êtes familier avec son fonctionnement. Les méthodes de gestion, l'observabilité et certains concepts sont différents des déploiements traditionnels. La méthode du diagramme de barre est destinée aux déploiements Vanilla Kubernetes et l'opérateur GitLab peut être utilisé pour déployer GitLab sur un cluster OpenShift. L'opérateur GitLab peut être utilisé pour automatiser les opérations du jour 2 dans les déploiements OpenShift et Kubernetes vanille.
    cards:
      - title: Tableau de bord
        subtext: Installer GitLab à l'aide des cartes Helm
        icon:
          name: kubernetes
          variant: marketing
          alt: Kubernetes Icon
          hex_color: '#336CE4'
        link_text: Voir les instructions d'installation
        link_url: https://docs.gitlab.com/charts/
        data_ga_name: helm charts
        data_ga_location: kubernetes installation
      - title: Opérateur GitLab
        new_flag: true
        subtext: Installer GitLab à l'aide de l'opérateur
        icon:
          name: gitlab-operator
          variant: marketing
          alt: GitLab Operator Icon
        link_text: Voir les instructions d'installation
        link_url: https://docs.gitlab.com/charts/installation/operator.html
        data_ga_name: gitlab operator
        data_ga_location: kubernetes installation
  supported_cloud:
    title: Nuage pris en charge
    text: |
      Utiliser le paquetage Linux officiel pour installer GitLab chez différents fournisseurs de services en nuage.
    cards:
      - title: Amazon Web Services (AWS)
        subtext: Installer GitLab sur AWS
        icon:
          name: aws
          variant: marketing
          alt: AWS Icon
        link_text: Voir les instructions d'installation
        link_url: https://docs.gitlab.com/ee/install/aws/
        data_ga_name: aws install documentation
        data_ga_location: supported cloud
      - title: Google Cloud Platform (GCP)
        subtext: Installer GitLab sur GCP
        icon:
          name: gcp
          variant: marketing
          alt: GCP Icon
        link_text: Voir les instructions d'installation
        link_url: https://docs.gitlab.com/ee/install/google_cloud_platform/
        data_ga_name: gcp install documentation
        data_ga_location: supported cloud
      - title: Microsoft Azure
        subtext: Installer GitLab sur Azure
        icon:
          name: azure
          variant: marketing
          alt: Azure Icon
        link_text: Voir les instructions d'installation
        link_url: https://docs.gitlab.com/ee/install/azure/
        data_ga_name: azure install documentation
        data_ga_location: supported cloud
  official_methods:
    title: Autres méthodes d'installation officielles et prises en charge
    cards:
      - title: Docker
        subtext: Images Docker officielles de GitLab
        icon:
          name: docker
          variant: marketing
          alt: Docker Icon
        link_text: Voir les instructions d'installation
        link_url: https://docs.gitlab.com/ee/install/docker.html
        data_ga_name: docker install documentation
        data_ga_location: official installation
      - title: Architectures de référence
        subtext: Topologies de déploiement recommandées pour GitLab
        icon:
          name: gitlab
          variant: marketing
          alt: GitLab Icon
        link_text: Voir les instructions d'installation
        link_url: https://docs.gitlab.com/ee/administration/reference_architectures/index.html
        data_ga_name: reference architectures install documentation
        data_ga_location: official installation
      - title: Installation à partir de la source
        subtext: Installer GitLab en utilisant les fichiers sources sur un système Debian/Ubuntu
        icon:
          name: source
          variant: marketing
          alt: Source Icon
        link_text: Voir les instructions d'installation
        link_url: https://docs.gitlab.com/ee/install/installation.html
        data_ga_name: installation from source
        data_ga_location: official installation
      - title: Boîte à outils de l'environnement GitLab (GET)
        subtext: Automatisation du déploiement des architectures de référence GitLab à l'aide de Terraform et Ansible
        icon:
          name: gitlab-environment-toolkit
          variant: marketing
          alt: GitLab Environment Toolkit Icon
        link_text: Voir les instructions d'installation
        link_url: https://gitlab.com/gitlab-org/gitlab-environment-toolkit
        data_ga_name: gitlab environment toolkit installation
        data_ga_location: official installation
  unofficial_methods:
    title: Méthodes d'installation non officielles et non soutenues
    cards:
      - title: Paquet natif Debian
        subtext: par Pirate Praveen
        icon:
          name: debian
          variant: marketing
          alt: Debian Icon
          hex_color: '#000'
        link_text: Voir les instructions d'installation
        link_url: https://wiki.debian.org/gitlab/
        data_ga_name: debian native installation
        data_ga_location: unofficial installation
      - title: Paquet FreeBSD
        subtext: par Torsten Zühlsdorff
        icon:
          name: freebsd
          variant: marketing
          alt: FreeBSD Icon
        link_text: Voir les instructions d'installation
        link_url: http://www.freshports.org/www/gitlab-ce
        data_ga_name: freebsd installation
        data_ga_location: unofficial installation
      - title: Paquet Arch Linux
        subtext: par la communauté Arch Linux
        icon:
          name: arch-linux
          variant: marketing
          alt: Arch Linux Icon
        link_text: Voir les instructions d'installation
        link_url: https://www.archlinux.org/packages/community/x86_64/gitlab/
        data_ga_name: arch linux installation
        data_ga_location: unofficial installation
      - title: Module Puppet
        subtext: par Vox Pupuli
        img_src: /nuxt-images/install/puppet-logo.svg
        icon:
          name: puppet
          variant: marketing
          alt: Puppet Icon
        link_text: Voir les instructions d'installation
        link_url: https://forge.puppet.com/puppet/gitlab
        data_ga_name: puppet module installation
        data_ga_location: unofficial installation
      - title: Manuel de jeu Ansible
        subtext: par Jeff Geerling
        icon:
          name: ansible
          variant: marketing
          alt: Ansible Icon
        link_text: Voir les instructions d'installation
        link_url: https://github.com/geerlingguy/ansible-role-gitlab
        data_ga_name: ansible installation
        data_ga_location: unofficial installation
      - title: Appareil virtuel GitLab (KVM)
        subtext: par OpenNebula
        icon:
          name: open-nebula
          variant: marketing
          alt: Open Nebula Icon
        link_text: Voir les instructions d'installation
        link_url: https://marketplace.opennebula.io/appliance/6b54a412-03a5-11e9-8652-f0def1753696
        data_ga_name: opennebula installation
        data_ga_location: unofficial installation
      - title: GitLab sur Cloudron
        subtext: via Cloudron App Library
        img_src: /nuxt-images/install/cloudron-logo.svg
        icon:
          name: cloudron
          variant: marketing
          alt: Cloudron Icon
        link_text: Voir les instructions d'installation
        link_url: https://cloudron.io/store/com.gitlab.cloudronapp.html
        data_ga_name: cloudron installation
        data_ga_location: unofficial installation
  updates:
    title: Vous avez déjà installé GitLab ?
    cards:
      - title: Mise à jour d'une ancienne version de GitLab
        text: Mettez à jour votre installation de GitLab pour bénéficier des dernières fonctionnalités. Les versions de GitLab qui incluent de nouvelles fonctionnalités sont publiées chaque mois le 22.
        link_url: https://about.gitlab.com/update/
        link_text: Mise à jour vers la dernière version de GitLab
        data_ga_name: Update to the latest relase of GitLab
        data_ga_location: update
      - title: Mise à jour de GitLab Community Edition
        text: GitLab Enterprise Edition comprend des caractéristiques et des fonctionnalités avancées qui ne sont pas disponibles dans la Community Edition.
        link_url: https://about.gitlab.com/upgrade/
        link_text: Passer à l'édition Enterprise
        data_ga_name: Upgrade to Enterprise Edition
        data_ga_location: update
      - title: Mise à jour à partir d'un paquet Omnibus installé manuellement
        text: Avec GitLab 7.10, nous avons introduit des dépôts de paquets pour GitLab, qui vous permettent d'installer GitLab avec une simple commande.
        link_url: https://about.gitlab.com/upgrade-to-package-repository/
        link_text: Mise à jour vers le dépôt de paquets Omnibus
        data_ga_name: Upgrade to Omnibus package repository
        data_ga_location: update
      - title: Applications tierces compatibles avec GitLab
        text: GitLab est ouvert à la collaboration et s'engage à établir des partenariats technologiques dans l'écosystème DevOps. En savoir plus sur les avantages et les conditions à remplir pour devenir un partenaire technologique de GitLab.
        link_url: https://about.gitlab.com/partners/
        link_text: Voir les applications tierces
        data_ga_name: View third-party applications
        data_ga_location: update
