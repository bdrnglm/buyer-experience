---
  title:  Software. Faster
  description: GitLab simplifies your DevSecOps so you can focus on what matters. Learn more!
  title_image: /nuxt-images/open-graph/open-graph-gitlab.png
  twitter_image: /nuxt-images/open-graph/open-graph-gitlab.png
  hero:
    title: |
      The faster path from idea to software
    aos_animation: fade-down
    aos_duration: 1600
    aos_offset: 0
    image:
      url: /nuxt-images/software-faster/hero.png
      alt: software-faster hero image
      aos_animation: fade-up
      aos_duration: 1600
      aos_offset: 0
    secondary_button:
      video_url: https://player.vimeo.com/video/799236905?h=59b06b0e99&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479
      text: What is GitLab?
      data_ga_name: watch video
      data_ga_location: hero
  customer_logos:
    showcased_enterprises:
      - image_url: "/nuxt-images/home/logo_tmobile_mono.svg"
        link_label: Link to T-Mobile and GitLab webcast landing page"
        alt: "T-Mobile logo"
        url: https://learn.gitlab.com/c/learn-how-t-mobile-i?x=04KSqy
      - image_url: "/nuxt-images/home/logo_goldman_sachs_mono.svg"
        link_label: Link to Goldman Sachs customer case study
        alt: "Goldman Sachs logo"
        url: "/customers/goldman-sachs/"
      - image_url: "/nuxt-images/home/logo_cncf_mono.svg"
        link_label: Link to Cloud Native Computing Foundation customer case study
        alt: "Cloud Native logo"
        url: /customers/cncf/
      - image_url: "/nuxt-images/home/logo_siemens_mono.svg"
        link_label: Link to Siemens customer case study
        alt: "Siemens logo"
        url: /customers/siemens/
      - image_url: "/nuxt-images/home/logo_nvidia_mono.svg"
        link_label: Link to Nvidia customer case study
        alt: "Nvidia logo"
        url: /customers/nvidia/
      - alt: UBS Logo
        image_url: "/nuxt-images/home/logo_ubs_mono.svg"
        url: https://about.gitlab.com/blog/2021/08/04/ubs-gitlab-devops-platform/
        link_label: Link to UBS customer case study
  featured_content:
    col_size: 4
    header: "Better together: Customers ship software faster with GitLab"
    case_studies:
      - header: Nasdaq's fast, seamless transition to the cloud
        description: |
          Nasdaq has a vision of becoming 100% cloud. They are partnering with GitLab to get there.
        showcase_img:
          url: /nuxt-images/software-faster/nasdaq-showcase.png
          alt: Nasdaq logo on a window
        logo_img:
          url: /nuxt-images/enterprise/logo-nasdaq.svg
          alt: Nasdaq logo
        link:
          video_url: https://player.vimeo.com/video/767082285?h=e76c380db4
          text: Watch the video
          data_ga_name: nasdaq
          data_ga_location: body
        aos_animation: fade-up
        aos_duration: 800
      - header: Achieve 5x faster deployment time
        description: |
          Hackerone improved pipeline time, deployment speed, and developer efficiency with GitLab Ultimate.
        showcase_img:
          url: /nuxt-images/software-faster/hackerone-showcase.png
          alt: Person working on a computer with code - HackerOne
        logo_img:
          url: /nuxt-images/logos/hackerone-logo.png
          alt: HackerOne logo
        link:
          href: /customers/hackerone/
          text: Learn more
          data_ga_name: hackerone
          data_ga_location: body
        aos_animation: fade-up
        aos_duration: 1000
      - header: Release features 144x faster
        description: |
          Airbus Intelligence improved their workflow and code quality with single application CI.
        showcase_img:
          url: /nuxt-images/software-faster/airbus-showcase.png
          alt: Airplane wing on flight - Airbus
        logo_img:
          url: /nuxt-images/software-faster/airbus-logo.png
          alt: Airbus logo
        link:
          href: /customers/airbus/
          text: Read their story
          data_ga_name: airbus
          data_ga_location: body
        aos_animation: fade-up
        aos_duration: 1200
  devsecops:
    header: All the essential DevSecOps tools in one comprehensive platform
    link:
      text: Learn more
      data_ga_name: platform
      data_ga_location: body
    cards:
      - header: Better insights
        description: End-to-end visibility across the software delivery lifecycle.
        icon: case-study-alt
      - header: Greater efficiency
        description: Built-in support for automation and integrations with 3rd services.
        icon: principles
      - header: Improved collaboration
        description: One workflow that unites developer, security, and ops teams.
        icon: roles
      - header: Faster time to value
        description: Continuous improvement through accelerated feedback loops.
        icon: verification
  by_industry_case_studies:
    title: Explore DevSecOps resources
    charcoal_bg: true
    header_animation: fade-up
    header_animation_duration: 500
    row_animation: fade-right
    row_animation_duration: 800
    rows:
      - title: 2023 Global DevSecOps Report Series
        subtitle: See what we learned from more than 5,000 DevSecOps professionals on the current state of software development, security, and operations.
        image:
          url: /nuxt-images/software-faster/devsecops-survey.svg
          alt: devsecops survey icon
        button:
          href: /developer-survey/
          text: Read the report
          data_ga_name: devsecops survey
          data_ga_location: body
        icon:
          name: doc-pencil-alt
          alt: Play Circle Icon
      - title: 'Cadence is Everything: 10x engineering organizations for 10x engineers.'
        subtitle: GitLab CEO and co-founder Sid Sijbrandij on the importance of cadence in engineering organizations.
        image:
          url: /nuxt-images/blogimages/athlinks_running.jpg
          alt: picture of people running marathon
        button:
          href: /blog/2022/11/03/cadence-is-everything-10x-engineering-organizations-for-10x-engineers/
          text: Read the blog
          data_ga_name: read the blog
          data_ga_location: body
        icon:
          name: blog
          alt: Play Circle Icon
