---
  title: Continuous integration in Agile development
  description: Learn more about how continuous integration and Agile methodologies
  topics_header:
    data:
      title: Continuous integration in Agile development
      block:
        - metadata:
            id_tag: cicd-pipeline
          text: |
            Learn more about how continuous integration and Agile methodologies work together.
  crumbs:
    - title: Topics
      href: /topics/
      data_ga_name: topics
      data_ga_location: breadcrumb
    - title: CI CD
      href: /topics/ci-cd/
      data-ga-name: ci-cd
      data_ga_location: breadcrumb
    - title: Continuous integration in Agile development
  side_menu:
    anchors:
      text: "On this page"
      data:
        - text: Agile core concepts
          href: "#agile-core-concepts"
          data_ga_name: agile core concepts
          data_ga_location: side-navigation
        - text: 'CI workflow in Agile'
          href: "#ci-workflow-in-agile"
          data_ga_name: ci best practice optimize pipeline stages
          data_ga_location: side-navigation
          nodes:
            - text: Using labels
              href: "#Using labels"
              data_ga_name: using labels
              data_ga_location: side-navigation
            - text: Code and commit changes
              href: "#Code and commit changes"
              data_ga_name: code and commit changes
              data_ga_location: side-navigation
            - text: Using Issue Boards between teams
              href: "#Using Issue Boards between teams"
              data_ga_name: using issue boards between teams
              data_ga_location: side-navigation
            - text: Deploying code
              href: "#Deploying code"
              data_ga_name: deploying code
              data_ga_location: side-navigation
        - text: Why CI is essential for Agile development
          href: "#why-ci-is-essential-for-agile-development"
          data_ga_name: 'ci best practice make builds fast and simple'
          data_ga_location: side-navigation
    content:
      - name: topics-copy-block
        data:
          column_size: 11
          blocks:
            - image:
                  image_url: /nuxt-images/blogimages/team-teams2.png
                  top: true
                  alt: team of teams of teams
              text: |
                  Agile development is a collection of methodologies that focus on iterative
                  development and collaboration between self-organized teams. Agile methods rely
                  on disciplined project management processes and cooperation between teams, or
                  even teams of teams.

                  While there are many Agile frameworks to choose from, all [Agile development](/solutions/agile-delivery/) projects involve some sort of [continuous development](/topics/ci-cd/), such as continuous planning, continuous testing, and continuous integration (CI).


                  With CI, developers gradually grow a stable system by working in small batches and short cycles, which is a lean concept. Continuous integration supports teams working within Agile frameworks with CI/CD pipelines that automate builds, tests, and deployments. While Agile falls into [Project and Portfolio Management](https://about.gitlab.com/blog/2020/11/11/gitlab-for-agile-portfolio-planning-project-management/) (PMM), CI is the process by which software development teams implement changes.


                  Fundamentally, [Scrum](https://www.scrum.org/resources/what-is-scrum) and other Agile methodologies are a management framework, not software engineering practices. Teams need to combine Agile frameworks with software engineering best practices to achieve the best results. Continuous integration puts Agile’s iterative development into action.
      - name: topics-copy-block
        data:
          header: Agile core concepts
          column_size: 11
          blocks:
            - text: |
                Agile project management delivers complex projects by focusing on incremental change. This iterative methodology has six core elements to track progress and create the product.


                * **User stories:** Describes the goals for the product written from the user’s perspective.

                * **Roadmap:** The high-level view of the requirements needed to fulfill the product vision.

                * **Backlog:** Requirements for the project by priority.

                * **Release plan:** A timetable for the release of a *working* product.

                * **Sprint/Iteration:** The user stories, goals, and tasks linked to the current sprint.

                * **Increment:** The working product presented to the stakeholders at the end of a sprint.


                Agile methodologies can vary in their approach but stay true to this model. In GitLab, project management features coincide with these Agile core concepts.


                | **Agile concept**  | **GitLab feature**                                                                                                                                                                |
                | ------------------ | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
                | User stories       | [Issues](https://docs.gitlab.com/ee/user/project/issues/)                                                                                                                         |
                | Tasks              | [Task lists](https://docs.gitlab.com/ee/user/markdown.html#task-lists)                                                                                                            |
                | Epics              | [Epics](https://docs.gitlab.com/ee/user/group/epics/)                                                                                                                             |
                | Points/Estimations | [Weights](https://docs.gitlab.com/ee/user/project/issues/issue_weight.html)                                                                                                       |
                | Backlog            | [Issue lists](https://docs.gitlab.com/ee/user/project/issues/#issues-per-project) and [prioritized labels](https://docs.gitlab.com/ee/user/project/labels.html#prioritize-labels) |
                | Sprints            | [Milestones](https://docs.gitlab.com/ee/user/project/milestones/)                                                                                                                 |
                | Burndown charts    | [Burndown charts](https://docs.gitlab.com/ee/user/project/milestones/burndown_and_burnup_charts.html)                                                                                        |
                | Agile boards       | [Issue boards](https://docs.gitlab.com/ee/user/project/issue_board.html)                                                                                                          |
      - name: topics-copy-block
        data:
          header: CI workflow in Agile
          column_size: 11
          blocks:
            - text: |
                Git it is the most popular method of [version control](https://about.gitlab.com/topics/version-control/), and can be beneficial for Agile teams because it allows for decentralized and simultaneous development. Tools like GitLab and others create a process around code mainly through issues and merge requests.


                A [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/index.html#merge-requests) (MR) is a way to visualize and collaborate on proposed changes to source code. A [CI workflow](https://about.gitlab.com/topics/version-control/what-is-gitlab-flow/) for teams using an Agile methodology would incorporate Agile core concepts as well as [commits](https://about.gitlab.com/blog/2018/06/07/keeping-git-commit-history-clean/), merge requests, testing, and deployments.


                ### Using labels


                Labels are a way for teams to filter and manage epics, issues, and merge requests. For a specific project, teams may have labels such as `Planning`, `In Progress`, `Staging`, `Reviewed`, etc with their own lists in the Issue Board.


                Teams can collaborate in the issues, and once next steps have been decided, a developer assigned to the issue might begin work and remove the label `Planning` and replace it with the label `In Progress`.


                ### Code and commit changes


                The developer can reference the corresponding issue in his commit message. The developer then pushes his commits to a [feature branch](https://docs.gitlab.com/ee/topics/git/) and creates a merge request. The code is built and tested, and a team member can review the code changes.


                ### Using Issue Boards between teams


                Once one team finishes their review, they can remove the label `In Progress` and move the issue to the `Staging` Issue Board. This can alert the deployment team that the issue is ready.


                ### Deploying code


                Once the MR has been approved, it can be merged into the main branch and the issue closed.
      - name: topics-copy-block
        data:
          header: Why CI is essential for Agile development
          column_size: 11
          blocks:
            - text: |             
                [Continuous integration](https://about.gitlab.com/topics/ci-cd/) drives development teams to implement small, frequent changes and validate code against version control repositories. In fact, CI can be considered a [pillar](https://www.browserstack.com/guide/continuous-integration-with-agile) of the Agile process. CI is so widely used that CI and Agile are often used interchangeably. While the concepts similarly stress small, iterative changes, they are not the same.


                Agile is built on the expectation that developers can deliver small incremental updates to a product or service. In practice, this will only be achieved if an organization commits to CI/CD automation. Agile frameworks allow teams to move quickly and work independently, but if the org does not actively use CI/CD, then Agile methods are essentially futile.


                Agile development strives to have a working product at the end of each Sprint. As the product goes through each Sprint, it’s improved exponentially. With CI, code changes made to the product are continually validated against other changes. Continuous integration and continuous testing ensures that teams can deliver this working product because bugs and potential issues are caught before they reach the end user.
  components:
    - name: solutions-resource-cards
      data:
        title: Continuous integration best practices
        column_size: 4
        cards:
          - icon:
              name: article
              variant: marketing
              alt: Article Icon
            event_type: "Article"
            header: Burndown and burnup charts
            image: "/nuxt-images/resources/resources_18.jpg"
            link_text: "Learn more"
            href: https://docs.gitlab.com/ee/user/project/milestones/burndown_and_burnup_charts.html
            data_ga_name: burndown and burnup charts
            data_ga_location: body
          - icon:
              name: article
              variant: marketing
              alt: Article Icon
            event_type: "Article"
            header: Milestones as Agile sprints
            image: "/nuxt-images/resources/resources_5.jpeg"
            link_text: "Learn more"
            href: https://docs.gitlab.com/ee/user/project/milestones/#milestones-as-agile-sprints
            data_ga_name: milestones as Agile sprints
            data_ga_location: body
    - name: solutions-resource-cards
      data:
        title: Suggested Content
        column_size: 4
        cards:
          - icon:
              name: web
              variant: marketing
              alt: Web Icon
            event_type: "Web"
            header: "How to use GitLab for Agile software development"
            text: |
                  How Agile artifacts map to GitLab features and how an Agile iteration looks in GitLab.
            link_text: "Learn more"
            href: https://about.gitlab.com/blog/2018/03/05/gitlab-for-agile-software-development/
            image: /nuxt-images/blogimages/autodevops.jpg
            data_ga_name: "How to use GitLab for Agile software development"
            data_ga_location: resource cards
          - icon:
              name: web
              variant: marketing
              alt: Web Icon
            event_type: "Web"
            header: "How to use GitLab for Agile, CI/CD, GitOps, and more"
            text: |
                  Read our example engineering stories from the past two years that show how to use GitLab for you DevOps cycle, including GitOps, CI/CD and more.
            link_text: "Learn more"
            href: https://about.gitlab.com/blog/2020/12/17/gitlab-for-cicd-agile-gitops-cloudnative/
            image: /nuxt-images/blogimages/conversicaimage.jpg
            data_ga_name: "How to use GitLab for Agile, CI/CD, GitOps, and more"
            data_ga_location: resource cards
          - icon:
              name: web
              variant: marketing
              alt: Web Icon
            event_type: "Web"
            header: "How to use GitLab for Agile portfolio planning and project management"
            text: |
                  GitLab provides features that are flexible enough to be used for scaled Agile portfolio planning and project management, regardless of the framework you choose.
            link_text: "Learn more"
            href: https://about.gitlab.com/blog/2020/11/11/gitlab-for-agile-portfolio-planning-project-management/
            image: /nuxt-images/blogimages/agilemultipleteams.jpg
            data_ga_name: "How to use GitLab for Agile portfolio planning and project management"
            data_ga_location: resource cards
