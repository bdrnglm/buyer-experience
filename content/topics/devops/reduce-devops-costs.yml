---
  title: Five ways to reduce the cost of a DevOps platform
  description: Even the most successful DevOps implementations can be streamlined could be more efficient. Here are five areas to consider to make your DevOps platform more cost efficient.
  partenttopic: devops
  twitter_image: /nuxt-images/open-graph/gitlab-blog-cover.png
  topics_header:
      data:
        title:  Five ways to reduce the cost of a DevOps platform
        block:
          - metadata:
              id_tag: reduce-devops-costs
            text: |
              Even the most successful DevOps implementations can be streamlined could be more efficient. Here are five areas to consider to make your DevOps platform more cost efficient.
  crumbs:
    - title: Topics
      href: /topics/
      data_ga_name: topics
      data_ga_location: breadcrumb
    - title: DevOps
      href: /topics/devops/
      data_ga_name: devops
      data_ga_location: breadcrumb
    - title: Five ways to reduce the cost of a DevOps platform
  side_menu:
    anchors:
      text: ""
      data: []
    content:
      - name: topics-copy-block
        data:
          no_header: true
          column_size: 12
          blocks:
            - text: |
                A DevOps platform brings obvious benefits to an organization - faster cycle times, better security, and generally happier developers. But successful DevOps teams want to go that one step further and actually reduce the costs of software development. DevOps platform teams already have a head start because they're no longer spending money and time to support and maintain multiple toolchains.


                But clearly more work can be done to reduce the costs of a DevOps platform. Here are five areas to consider:


                **1. The cloud:** All that flexibility provided by the cloud has a potential dark side - cloud sprawl. With anyone able to spin up instances on a credit card it's a little too easy to “set it and forget it.” If left unchecked, cloud costs can be even greater than on-prem. [Cloud pricing includes other factors](https://enterprisersproject.com/article/2018/7/cloud-costs-4-myths-and-misunderstandings) like storage, networking, monitoring, and backups, among other services.


                Cloud sprawl can also refer to SaaS instances, such as Salesforce, Adobe, or any other online service, where an organization pays for new user accounts, but [doesn't actually use them](https://searchcloudcomputing.techtarget.com/definition/cloud-sprawl). Monitoring cloud usage can give some insights on a stretched DevOps budget.


                **2. The toolchain:** Even a solid DevOps platform can have some unnecessary services hanging around, so it pays to take a look at everything you have a license for to ensure it's all being used. If you're still unsure, our [toolchain calculator](/calculator/roi/){data-ga-name="toolchain calculator" data-ga-location="body"} breaks it all down clearly.


                **3. Legacy systems:** Aging infrastructure gorges itself on money, both in terms of upkeep and maintenance. If you don't believe that, try [hiring a COBOL developer](/blog/2020/04/23/cobol-programmer-shortage/){data-ga-name="cobol developer" data-ga-location="body"} at an average developer salary (spoiler alert: you can't). When you hear about leading companies like Amazon and Facebook, they have an advantage: They can build relatively new systems and DevOps capabilities into their applications because they don't have legacy systems to worry about. Over time, the true cost of legacy systems is enormous: From additional resources needed to maintain them to lost productivity, they can hinder investments in long-term growth - the thing that will increase revenue in the long run.


                **4. Manual tasks:** Even a well-established DevOps team will admit that not everything is automated…but it should be. Nowhere is that more true than when it comes to testing. In our [2020 Global DevSecOps Survey](/developer-survey/){data-ga-name="devsecops survey" data-ga-location="body"}, 47% of respondents pointed to testing as the most likely reason for release delays, that's down just 2% from 2019. Everyone agrees more testing needs to be done all over the place, but test automation remains a work in progress at most companies; our survey found just 12% of respondents reported tests are fully automated.


                **5. Time wasters:** Time equals money, so a final step toward reducing the costs of DevOps must include looking at inefficient processes. One obvious place to start is code review. Our survey showed code review is both vital to successful DevOps and happening much more frequently (anecdotal reports show many teams are doing code reviews as frequently as daily). But the survey also showed intense frustration around code reviews: too many people involved, no clear process in place and a lack of agreement on its importance. In other words, code reviews are a time suck in many organizations and that leads to wasted money and missed opportunities.
      
      - name: topics-cta
        data:
          subtitle: Take a deep dive into GitLab's DevOps platform
          text: |
            GitLab Learn is organized into learning paths which cover different subjects and are each a collection of relevant videos and self driven demos. Let's get learning!
          column_size: 12
          cta_one:
            text: Visit GitLab Learn
            link: /learn/
            data_ga_name: visit GitLab Learn
            data_ga_location: body
  components:
    - name: solutions-resource-cards
      data:
        title: Read more about DevOps
        column_size: 4
        cards:
          - icon:
              name: video
              variant: marketing
              alt: video Icon
            event_type: "Video"
            header: How DevOps leads transformation (GitLab Virtual Commit 2020 track)
            image: "/nuxt-images/topics/devops/how-devops-leads-transformation.jpeg"
            link_text: "Watch now"
            href: https://www.youtube.com/playlist?list=PLFGfElNsQthbAbiHjRVNz1WwxbhLfeXXs
            data_ga_name: How DevOps leads transformation 
            data_ga_location: body
          - icon:
              name: video
              variant: marketing
              alt: video Icon
            event_type: "Video"
            header: Cloud-Native DevOps (GitLab Virtual Commit 2020 track)
            image: "/nuxt-images/topics/devops/cloud-native-devops.jpeg"
            link_text: "Watch now"
            href: https://www.youtube.com/playlist?list=PLFGfElNsQthb4FD4y1UyEzi2ktSeIzLxj
            data_ga_name: Cloud-Native DevOps
            data_ga_location: body
          - icon:
              name: video
              variant: marketing
              alt: video Icon
            event_type: "Video"
            header: DevOps tips and tricks (GitLab Virtual Commit 2020 track)
            image: "/nuxt-images/topics/devops/devops-tips-and-tricks.jpeg"
            link_text: "Watch now"
            href: https://www.youtube.com/playlist?list=PLFGfElNsQthZ_LGh4EpGJduNd2nFhN5fn
            data_ga_name: DevOps tips and tricks
            data_ga_location: body
          - icon:
              name: video
              variant: marketing
              alt: video Icon
            event_type: "Video"
            header: How to simplify DevOps
            image: "/nuxt-images/topics/devops/simplify-devops.jpeg"
            link_text: "Watch now"
            href: https://www.youtube.com/watch?v=TUwvgz-wsF4
            data_ga_name: How to simplify DevOps
            data_ga_location: body
          - icon:
              name: article
              variant: marketing
              alt: Article Icon
            event_type: "Article"
            header: Understand shift left DevOps
            image: "/nuxt-images/resources/resources_18.jpg"
            link_text: "Learn more"
            href: /topics/ci-cd/shift-left-devops/
            data_ga_name: Understand shift left DevOps
            data_ga_location: body
    - name: solutions-resource-cards
      data:
        title: Suggested Content
        column_size: 4
        cards:
          - icon:
              name: web
              variant: marketing
              alt: Web Icon
            event_type: "Web"
            header: "Autoscale GitLab CI/CD runners and save 90% on EC2 costs"
            text: |
                Guest author Max Woolf shows how his team makes big savings with an autoscaling cluster of GitLab CI/CD runners.
            link_text: "Learn more"
            href: /blog/2017/11/23/autoscale-ci-runners/
            image: /nuxt-images/blogimages/devops-tool-landscape.jpeg
            data_ga_name: "Autoscale GitLab CI/CD runners and save 90% on EC2 costs"
            data_ga_location: resource cards
          - icon:
              name: web
              variant: marketing
              alt: Web Icon
            event_type: "Web"
            header: "Auto DevOps 101: How we're making CI/CD easier"
            text: |
                VP of product strategy Mark Pundsack shares everything you need to know about Auto DevOps.
            link_text: "Learn more"
            href: /blog/2019/10/07/auto-devops-explained/
            image: /nuxt-images/blogimages/beginners-guide-to-ci.jpg
            data_ga_name: "Auto DevOps 101: How we're making CI/CD easier"
            data_ga_location: resource cards
          - icon:
              name: web
              variant: marketing
              alt: Web Icon
            event_type: "Web"
            header: "How a GitLab engineer changed the future of DevOps"
            text: |
                When Kamil Trzciński suggested we integrate GitLab version control and GitLab CI one into a single product, GitLab's pioneering DevOps Platform was born.
            link_text: "Learn more"
            href: /blog/2020/10/29/gitlab-hero-devops-platform/
            image: /nuxt-images/resources/resources_17.jpg
            data_ga_name: "How a GitLab engineer changed the future of DevOps"
            data_ga_location: resource cards
