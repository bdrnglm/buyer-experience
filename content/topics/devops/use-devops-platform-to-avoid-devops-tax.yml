---
  title: Choose a DevOps platform to avoid the DevOps tax
  description: Too many tools/toolchains can impose a hefty DevOps tax on your organization. Here's how a single DevOps platform can lighten your burden.
  partenttopic: devops
  twitter_image: /nuxt-images/open-graph/gitlab-blog-cover.png
  topics_header:
      data:
        title:  Choose a DevOps platform to avoid the DevOps tax
        block:
          - metadata:
              id_tag: use-devops-platform-to-avoid-devops-tax
            text: |
              Too many tools/toolchains can impose a hefty DevOps tax on your organization. Here's how a single DevOps platform can lighten your burden.
  crumbs:
    - title: Topics
      href: /topics/
      data_ga_name: topics
      data_ga_location: breadcrumb
    - title: Devops
      href: /topics/devops/
      data_ga_name: devops
      data_ga_location: breadcrumb
    - title: Choose a DevOps platform to avoid the DevOps tax
  side_menu:
    anchors:
      text: "On this page"
      data:
        - text: "To avoid the DevOps tax, here's what to consider:"
          href: "#to-avoid-the-dev-ops-tax-here-s-what-to-consider"
          data_ga_name: "to avoid the DevOps tax, here's what to consider:"
          data_ga_location: side-navigation
          variant: primary
    content:
      - name: topics-copy-block
        data:
          column_size: 10
          no_header: true
          blocks:
            - text: |
                [DevOps](/topics/devops/){data-ga-name="devops" data-ga-location="body"} proves that there really can be too much of a good thing. By tying all the parts of the software development lifecycle together - from planning to delivery - it's practically begging for tools to be cobbled together to do just that.


                But, administering all these products and connecting them together is complex. For example, your CI needs to talk to your version control, your code review, your security testing, your container registry, and your configuration management. The permutations are staggering, and it's not just a one-time configuration - each new project needs to reconnect all these pieces together.


                This phenomenon is so real that it has a name: the DevOps tax. A DevOps tax is the price teams pay for using multiple tools and/or multiple toolchains in order to speed up the delivery of software. That price is often looked at in manpower spent: How much time does a team have to spend integrating and maintaining a toolchain versus actually coding and delivering software?


                So what is a typical DevOps tax? A [Forrester Research report](https://go.forrester.com/blogs/the-rise-fall-and-rise-again-of-the-integrated-developer-tool-chain/) from 2019 indicated it was approximately 10%, meaning 10% of the team had to support and maintain the toolchain. Our [2020 Global DevSecOps Survey](https://about.gitlab.com/developer-survey/){data-ga-name="2020 developer survey" data-ga-location="body"} found it might be even higher: 22% of respondents said they spend between 11% and 20% of their time (monthly) supporting the toolchain.


                The solution to this problem is a [DevOps platform](/solutions/devops-platform/){data-ga-name="devops platform" data-ga-location="body"}, perhaps supported [by a platform team](/topics/devops/how-and-why-to-create-devops-platform-team/){data-ga-name="platform team" data-ga-location="body"}, that will streamline every aspect of the software development lifecycle.

      - name: topics-copy-block
        data:
          header: "To avoid the DevOps tax, here's what to consider:"
          column_size: 10
          blocks:
            - text: |
                * Start with a true platform, delivered as a single application. Gartner Group forecasts that by 2023, 40% of companies will standardize on a single DevOps platform (what Gartner currently refers to as a [DevOps value stream delivery platform](https://learn.gitlab.com/gartner-vsdp/gartner-mg-vsdp20).

                * Think about maintenance. How easy will it be to upgrade? Can upgrades be automated? How much manpower will it take to keep the platform running?

                * Choose a DevOps platform with APIs in mind. A DevOps platform doesn't mean an organization will only have a single tool; in fact, most companies need to choose a platform that can be easily integrated with existing tools whether it's a company-wide project management solution or something mandated by industry regulation. A DevOps platform with robust APIs for those types of integrations are a must. Ideally, a team should look for something with [off-the-shelf integration capabilities](https://amazicworld.com/devops-tool-sprawl-is-tool-tax-just-the-tip-of-the-iceberg/).

                * Consider a “future-facing” platform. From IoT to AI and ML, exciting new technologies are just around the corner, so your DevOps platform needs to be able to accommodate that

                * Insist on a 360 degree view into everything. Gartner Group recommends platforms that offer “enhanced visibility, traceability, auditability, and observability” across the entire spectrum of operations.

                * Don't forget to support communication and collaboration. GitLab's 2020 Survey found devs, security pros, ops team members, and testers were unanimous in their belief that communication and collaboration would be the most important skills for the future. Communication and collaboration are at the heart of so many stages of software development, from code review to UX and product planning, so choose a DevOps platform that support these efforts.


  components:
    - name: solutions-resource-cards
      data:
        title: Related Resources
        column_size: 4
        grouped: false
        cards:
          - icon:
              name: webcast
              variant: marketing
              alt: webcast Icon
            event_type: "Webcast"
            header: The hidden costs of DevOps toolchains
            link_text: "Watch now"
            image: "/nuxt-images/topics/devops/how-devops-leads-transformation.jpeg"
            href: /webcast/simplify-to-accelerate/
            data_ga_name: The hidden costs of DevOps toolchains
            data_ga_location: resource cards
          
          - icon:
              name: case-study
              variant: marketing
              alt: Case Study Icon
            event_type: "Case study"
            header: How BI Worldwide leveraged a DevOps platform
            link_text: "Learn more"
            image: "/nuxt-images/resources/resources_8.jpeg"
            href: /customers/bi_worldwide/
            data_ga_name: How BI Worldwide leveraged a DevOps platform
            data_ga_location: resource cards
          - icon:
              name: case-study
              variant: marketing
              alt: Case Study Icon
            event_type: "Case study"
            header: Glympse went from 20 tools to one
            link_text: "Learn more"
            image: "/nuxt-images/resources/resources_3.jpg"
            href: /customers/glympse/
            data_ga_name: Glympse went from 20 tools to one
            data_ga_location: resource cards
         
    - name: solutions-resource-cards
      data:
        title: Suggested Content
        column_size: 4
        cards:
          - icon:
              name: web
              variant: marketing
              alt: Web Icon
            event_type: "Web"
            header: "How to avoid the DevOps tax"
            text: |
                  Realize a faster DevOps lifecycle with these best practices for integration and automation - watch our recent webcast with guest speaker Forrester Senior Analyst Christoper Condo and GitLab Head of Product Mark Pundsack.
            link_text: "Learn more"
            href: /blog/2018/03/21/avoiding-devops-tax-webcast/
            image: /nuxt-images/blogimages/autodevops.jpg
            data_ga_name: "How to avoid the DevOps tax"
            data_ga_location: resource cards
          - icon:
              name: web
              variant: marketing
              alt: Web Icon
            event_type: "Web"
            header: "How pre-filled CI/CD variables will make running pipelines easier"
            text: |
                  Learn more about this future release and how pre-filled variables will save time and reduce errors.
            link_text: "Learn more"
            href: /blog/2020/12/02/pre-filled-variables-feature/
            image: /nuxt-images/blogimages/beginners-guide-to-ci.jpg
            data_ga_name: "How pre-filled CI/CD variables will make running pipelines easier"
            data_ga_location: resource cards
          - icon:
              name: web
              variant: marketing
              alt: Web Icon
            event_type: "Web"
            header: "CNCF's 5 technologies to watch in 2021"
            text: |
                  We predict how CNCF's five tech trends to watch will impact cloud native and the tech industry over the next year and beyond.
            link_text: "Learn more"
            href: /blog/2020/11/24/cncf-five-technologies-to-watch-in-2021/
            image: /nuxt-images/blogimages/scm-ci-cr.png
            data_ga_name: "CNCF's 5 technologies to watch in 2021"
            data_ga_location: resource cards
