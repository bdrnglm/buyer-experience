metadata:
  title: GitLab @ RSA Conference 2023
  og_title: GitLab @ RSA Conference 2023
  description: "Come by the GitLab booth and take a deep dive into our DevSecOps platform, learn best practices from product experts, get your technical questions answered, grab some swag, and let us know what you'd like to see in new releases."
  twitter_description: "Come by the GitLab booth and take a deep dive into our DevSecOps platform, learn best practices from product experts, get your technical questions answered, grab some swag, and let us know what you'd like to see in new releases."
  image_title: /nuxt-images/open-graph/gitlab-kubecon-opengraph.jpg
  og_description: "Come by the GitLab booth and take a deep dive into our DevSecOps platform, learn best practices from product experts, get your technical questions answered, grab some swag, and let us know what you'd like to see in new releases."
  og_image: /nuxt-images/open-graph/gitlab-kubecon-opengraph.jpg
  twitter_image: /nuxt-images/open-graph/gitlab-kubecon-opengraph.jpg

hero:
  headline: Come see GitLab at RSA 2023!
  accent: RSA Conference 2023 | San Francisco | April 24 - 27 | Moscone Center
  sub_heading: GitLab offers a breadth of security vulnerability identification and management tools
  list:
  - icon: magnifying-glass-eye.svg
    description: Scan the complete application lifecycle for vulnerabilities
  - icon: bulb-bolt-96px.svg
    description: Get detailed insights and solutions to vulnerabilities
  - icon: lock-close.svg
    description: Prevent insecure code from making it to production
  - icon: cog-user.svg
    description: Provide system wide governance
  - icon: clipboard-check.svg
    description: Easily adhere to compliance and audit requirements

intro_description: GitLab is the most comprehensive DevSecOps platform for software innovation and empowers developers to create secure code faster by enforcing security through your entire software development life cycle.

gitlab_booth:
  title: The GitLab Booth
  subheadings:
    - title: Location
      value: "North Expo Hall Booth #6371"
  information: "Come by the GitLab booth and take a deep dive into our DevSecOps platform, learn best practices from product experts, get your technical questions answered, grab some swag, and let us know what you'd like to see in new releases."
  image:
    src: "387A1087-compressed 1.jpg"
    alt: ''

featured_speaker:
  speaker:
    name: David DeSanto
    role: Chief Product Officer
    organization: GitLab
    image: /nuxt-images/events/rsa/headshot-david.png
  session:
    name: DevOps is Now DevSecOps
    content: >
      I don't want to say, “I told you so,” but ... over the years, I have said many times that the reason I got into DevOps was that I thought it was the best thing for security.
      At first, both the DevOps community and the security folks were reluctant to realize and acknowledge this.
      But now, almost 10 years later, it is becoming a more acceptable opinion.
      DevOps must have security baked in. Whether you put a Sec in between Dev and Ops is up to you, but security is here to stay as part of the DevOps way.
    date: April 24
    time: 2:30 PM - 3:00 pm
    location: Moscone South
    section: Room 308

happy_hour:
  title: You're Invited to our Happy Hour!
  subheadings:
    - title: Location
      value: Press Club - 20 Yerba Buena Lane, San Francisco CA 94103
    - title: Date
      value: April 26th
    - title: Time
      value: 6:30-9:30 PM PST

  information: |
    You are cordially invited to WINE down with us on April 26th at one of San Francisco’s hottest wine bars and lounges, just two blocks from Moscone!

    Join us for a night of mingling with like-minded individuals, delightful conversation, and a sip (or more) of some of the finest biodynamic, organically sourced, and hard-to-find varieties of wine.

    Space is limited, so RSVP now to secure your spot! See you there for a Happy Hour you won’t forget! 🥂
  call_to_action:
    button_text: Register for our party!
    button_link: https://goteleport.com/sf-happyhour/?utm_campaign=eg&utm_medium=Gitlab%20&utm_source=events
    data_ga_name: Happy hour registration
    data_ga_location: happy hour
  cropped: false
  alignTop: true
  image:
    src: happy-hour.png
    alt: RSA Happy Hour sponsored by Teleport, OpenRaven, and GitLab

featured_content:
  title: GitLab DevSecOps for everyone!
  description:
  - Whether you are a Developer or AppSec Engineer, GitLab is made for you.
  - GitLab is the industry's most comprehensive DevSecOps platform. GitLab
    enables teams to ship better, more secure software at lightning speeds. As
    a platform for the entire software development lifecycle, GitLab enables
    all stakeholders to collaborate in developing, securing and operating
    software.
  sections:
  - title: Put on your AppSec Hat!
    list:
    - Wide breadth of security tools for vulnerability identification, management, and remediation
    - Improve security risk mitigation and enhance compliance with robust security guardrails and policy enforcement
    - Gain system wide visibility with insights across the entire software development lifecycle
    - Secure your Software Supply-Chain with Dependency Management, SBOM generation, and Artifact Attestation
  - title: Put on your Developer Hat!
    list:
    - Scan your application’s complete lifecycle for vulnerabilities
    - Become more security aware through integrated security training
    - Resolve vulnerabilities with ease using detailed vulnerability insights
    - Single source of truth for vulnerabilities, reducing context switching and enabling collaboration with your security team
    - Leverage fuzz testing to find bugs and hidden vulnerabilities
    - Identify security threats before code merge, not weeks later
