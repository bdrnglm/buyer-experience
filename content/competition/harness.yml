---
  data:
    competitor: Harness
    gitlab_coverage: 50
    competitor_coverage: 50
    subheading: Harness is a Software Delivery Platform that leverages AI to simplify the DevOps processes, including CI, CD, Feature Flags, and Cloud Costs.
    comparison_table:
      - stage: Release
        features:
          - feature: Continuous Delivery
            gitlab:
              coverage: 100
              projected_coverage: 100
              description:  Deliver your changes to production with zero-touch software delivery; focus on building great software and allow GitLab CD to bring your release through your path to production for you.
              details: |
                * GitLab CD includes the creation of pipelines with CD-related stages in it.
                * GitLab has [Auto DevOps and Auto Deploy](https://docs.gitlab.com/ee/topics/release_your_application.html#deploy-with-auto-devops){data-ga-name="link to auto devops and auto deploy" data-ga-location="body"} reusable templates
                * GitLab Auto Deploy has [built-in support](https://docs.gitlab.com/ee/topics/release_your_application.html#deploy-to-aws-with-gitlab-cicd){data-ga-name="link to built-in support" data-ga-location="body"} for Amazon EC2 and ECS deployments, in addition to Kubernetes
                * GitLab CD leverages the same data model that other GitLab DevOps categories use and it is tightly integrated with them to generate higher-value insight to end users.
              improving_product_capabilities: |
                * GitLab’s CD proposed improvements are described in its category direction [page](https://about.gitlab.com/direction/release/continuous_delivery/#whats-next--why){data-ga-name="link to category direction page" data-ga-location="body"}.
                * Adding a Pipeline Studio editor similar to Harness would be very useful to pipeline creators.
              link_to_documentation: https://docs.gitlab.com/ee/ci/
            competitor:
              coverage: 100
              description: "Harness CD is a module that enables engineers to deploy on-demand without scripts, plugins, version dependencies, toil, downtime, and anger. With Harness, you can create pipelines visually or using code with their Pipeline Studio, which includes built-in steps (think of jobs), which you can drag-and-drop when visually creating pipelines."
              details: |
                * Harness [supports deploying to many deployment targets](https://docs.harness.io/article/220d0ojx5y-supported-platforms#deployments){data-ga-name="link to supports deploying to many deployment targets" data-ga-location="body"}, such as K8s, VMs, physical data center, all major cloud providers
                * Harness CD provides a variety of [connectors](https://docs.harness.io/category/o1zhrfo8n5){data-ga-name="link to connectors" data-ga-location="body"} to integrate to other third-party solutions, such as Jira, ServiceNow, GCP, AWS, Azure, etc.
                * Harness CD includes out-of-the-box [dashboards](https://docs.harness.io/article/phiv0zaoex-monitor-cd-deployments){data-ga-name="link to dashboards" data-ga-location="body"} but also provides wizards to create custom dashboards.
              additional: |
                * The First-Gen version of Harness CD is their most mature product.
              link_to_documentation: https://developer.harness.io/docs/first-gen/continuous-delivery/concepts-cd/deployments-overview/
          - feature: Advanced Deployments
            gitlab:
              coverage: 50
              projected_coverage: 50
              description: Mitigate the risk of production deploys by deploying new production code to a small subset of your fleet and then incrementally adding more.
              details: |
                * GitLab supports [canary](https://docs.gitlab.com/ee/user/project/canary_deployments.html){data-ga-name="link to canary" data-ga-location="body"}, [incremental](https://docs.gitlab.com/ee/ci/environments/incremental_rollouts.html){data-ga-name="link to incremental" data-ga-location="body"} and timed incremental via the Auto DevOps template.
                * GitLab does not support blue/green out-of-the-box
                * GitLab does not support built-in drag-and-drop visual steps that would aid you in creating advanced deployment techniques in your pipelines.
              improving_product_capabilities: |
                * GitLab is working towards developing more capabilities for advanced deployment techniques.
                * Adding something similar to Harness’s visual built-in steps for advanced deployment techniques that could be drag-and-dropped into pipelines.
              link_to_documentation: https://docs.gitlab.com/ee/topics/autodevops/index.html#incremental-rollout-to-production-premium
            competitor:
              coverage: 75
              description: Harness matches GitLab’s capabilities and adds built-in visual steps (and detailed documentation) that support the creation of advanced deployment techniques into your pipelines.
              details: |
                * Harness supports advanced deployment techniques, such as [rolling](https://docs.harness.io/article/325x7awntc-deployment-concepts-and-strategies#rolling_deployment){data-ga-name="link to rolling" data-ga-location="body"} (incremental), canary, [blue/green](https://docs.harness.io/article/7qtpb12dv1-ecs-blue-green-workflows){data-ga-name="link to blue/green" data-ga-location="body"}. For Next-Gen CD, check out this [doc link](https://docs.harness.io/article/xsla71qg8t-create-a-kubernetes-rolling-deployment){data-ga-name="link to doc link" data-ga-location="body"}.
                * Harness has good documentation on how to implement these advanced deployments for their [First-Gen](https://docs.harness.io/article/325x7awntc-deployment-concepts-and-strategies#rolling_deployment){data-ga-name="link to first-gen" data-ga-location="body"} and [Next-Gen](https://docs.harness.io/article/0zsf97lo3c){data-ga-name="link to next-gen" data-ga-location="body"} CD products.
              additional: |
                * Harness provides built-in steps (think of jobs), which you can drag-and-drop when creating pipelines, and instructions on how to create workflows for advanced deployment techniques. For example, [here is the documentation](https://docs.harness.io/article/7qtpb12dv1-ecs-blue-green-workflows){data-ga-name="link to blue/green documentation" data-ga-location="body"} to create a workflow (pipeline) for an ECS Blue/Green deployment.
                * Although not part of Harness CD, Harness Service Reliability Management (SRM) allows you to define [SLOs](https://docs.harness.io/article/d3m9uo4mx0-slo-driven-deployment-governance){data-ga-name="link to service reliability objectives" data-ga-location="body"} (Service Reliability Objectives), which leverage OPA (Open Policy Agent - think of a BRMS for K8s), to monitor deployment health and stop pipelines from deploying to a specific environment, for example. A good intro video on Harness SRM can be found [here](https://youtu.be/Q0Fv5_0_Nvg){data-ga-name="link to intro video on harness srm" data-ga-location="body"}. Harness SRM is Next-Gen and only works with Harness CD Next-Gen. In the future, Harness plans to extend Harness SRM to work with other CI/CD solutions in the market.
              link_to_documentation: https://docs.harness.io/category/cwefyz0jos-deployments-overview
          - feature: Feature Flags
            gitlab:
              coverage: 50
              projected_coverage: 50
              description: Feature flags enable teams to achieve CD by letting them deploy dark features to production as smaller batches for controlled testing, separating feature delivery from customer launch, and removing risk from delivery.
              details: |
                * GitLab has Progressive Delivery capabilities, e.g. [feature flags](https://docs.gitlab.com/ee/topics/release_your_application.html#feature-flags){data-ga-name="link to feature flags" data-ga-location="body"}.
                * GitLab Feature Flags is a capability that is part of the GitLab solution and is not offered as a separate module or sold separately. In fact, it’s part of our Free tier.
                * GitLab Feature Flags offers [SDK for many languages](https://github.com/Unleash/unleash#unleash-sdks){data-ga-name="link to sdk for many languages" data-ga-location="body"}.
              improving_product_capabilities: |
                * Feature Flags direction [page](https://about.gitlab.com/direction/release/feature_flags/#whats-next--why){data-ga-name="link to flags direction page" data-ga-location="body"}.
                * It would be nice to add analytics graphs for Feature Flags metrics, e.g usage metrics, such as # of times invoked, how they evaluated, last time invoked, per environment, etc.
              link_to_documentation: https://docs.gitlab.com/ee/operations/feature_flags.html
            competitor:
              coverage: 50
              description: Harness Feature Flags, a separate module from Harness CD, is very similar in functionality to GitLab Feature Flags.
              details: |
                * Harness Feature Flags module is offered and sold separately.
                * Harness Feature Flags works with the Next-Gen product version of Harness CD and not the First-Gen, which most of their customers use.
                * Harness includes Feature Flag [analytics](https://docs.harness.io/article/wb2nmcpo9x-view-metrics){data-ga-name="link to analytics" data-ga-location="body"}, such as usage metrics in graphical form
                * A Harness Feature Flag can have a [prerequisite](https://docs.harness.io/article/iijdahygdm-add-prerequisites-to-feature-flag){data-ga-name="link to prerequisite" data-ga-location="body"} (dependent on the result of another flag).
                * Harness Feature Flags offers [SDK for many languages](https://docs.harness.io/article/rvqprvbq8f-client-side-and-server-side-sdks#supported_application_types){data-ga-name="link to sdk for many languages" data-ga-location="body"}.
                * Harness Feature Flags supports [multivariate](https://docs.harness.io/article/1j7pdkqh7j-create-a-feature-flag#create_a_multivariate_flag){data-ga-name="link to multivariate" data-ga-location="body"} flags.
              additional: |
                * Your First Feature Flag in Harness [video](https://youtu.be/Zf51EDcDa80){data-ga-name="link to first feature flag in harness video" data-ga-location="body"}
                * [Video](https://harness-1.wistia.com/medias/h6iegycy0a){data-ga-name="link to video" data-ga-location="body"} showing Harness Feature Flags UI
              link_to_documentation: https://docs.harness.io/article/7n9433hkc0-cf-feature-flag-overview
          - feature: Release Evidence
            gitlab:
              coverage: 25
              projected_coverage: 25
              description: Release Evidence includes all the assurances and evidence collection that are necessary for you to trust the changes you’re delivering.
              details: |
                * Each time a release is created, GitLab takes a snapshot of data that’s related to it. This data is saved in a JSON file and called [release evidence](https://docs.gitlab.com/ee/user/project/releases/#release-evidence){data-ga-name="link to release evidence" data-ga-location="body"}.
              improving_product_capabilities: |
                * GitLab Release Evidence direction [page](https://about.gitlab.com/direction/release/release_evidence/){data-ga-name="link to evidence direction page" data-ga-location="body"}.
              link_to_documentation: https://docs.gitlab.com/ee/user/project/releases/#release-evidence
            competitor:
              coverage: 0
              description: Harness does not offer this
              details: |
                * Harness relies on integrating to git solutions for this, such as GitHub, GitLab, Bitbucket, AWS CodeCommit Repo, Azure DevOps Repo
          - feature: Release Orchestration
            gitlab:
              coverage: 50
              projected_coverage: 100
              description:  Management and orchestration of releases-as-code built on intelligent notifications, scheduling of delivery and shared resources, blackout periods, relationships, parallelization, and sequencing, as well as support for integrating manual processes and interventions.
              details: |
                * GitLab includes capabilities to [manage releases](https://docs.gitlab.com/ee/user/project/releases/){data-ga-name="link to manage releases" data-ga-location="body"} including the ability to [create upcoming releases](https://docs.gitlab.com/ee/user/project/releases/){data-ga-name="link to create upcoming releases" data-ga-location="body"} and [historical releases](https://docs.gitlab.com/ee/user/project/releases/){data-ga-name="link to historical releases" data-ga-location="body"}.
                * GitLab allows you to [create notifications](https://docs.gitlab.com/ee/user/project/releases/#get-notified-when-a-release-is-created){data-ga-name="link to create notifications" data-ga-location="body"} when a release is created.
                * GitLab supports the [prevention of unintentional releases](https://docs.gitlab.com/ee/user/project/releases/#prevent-unintentional-releases-by-setting-a-deploy-freeze){data-ga-name="link to prevention of unintentional releases" data-ga-location="body"}.
                * You can control what others can do with releases by [setting their permissions](https://docs.gitlab.com/ee/user/project/releases/#release-permissions){data-ga-name="link to setting their permissions" data-ga-location="body"}.
                * GitLab provides out-of-the-box [release metrics](https://docs.gitlab.com/ee/user/project/releases/#release-metrics){data-ga-name="link to release metrics" data-ga-location="body"}.
              improving_product_capabilities: |
                * Cleaning up stale environments - Environments cleanup [epic](https://gitlab.com/groups/gitlab-org/-/epics/5920){data-ga-name="link to environments cleanup epic" data-ga-location="body"}
                * Environments Search [epic](https://gitlab.com/groups/gitlab-org/-/epics/8467){data-ga-name="link to environments search epic" data-ga-location="body"}
                * Add Environments support to the GitLab Agent for K8s [issue](https://gitlab.com/gitlab-org/gitlab/-/issues/352186){data-ga-name="link to environments support to the GitLab Agent for K8s issue" data-ga-location="body"}
                * GitLab’s Releases direction [page](https://about.gitlab.com/direction/release/#whats-next-and-why){data-ga-name="link to gitlab's releases direction page" data-ga-location="body"}.
              link_to_documentation: https://docs.gitlab.com/ee/user/project/releases/
            competitor:
              coverage: 0
              description: Harness does not offer this
              details: |
                * Harness relies on integrating to git solutions for this, such as GitHub, GitLab, Bitbucket, AWS CodeCommit Repo, Azure DevOps Repo
          - feature: Environment Management
            gitlab:
              coverage: 50
              projected_coverage: 100
              description: Enable organizations to operate and manage multiple environments directly from GitLab. Environments are encapsulated in GitLab as a target system with associated configurations. By facilitating access control, visualizing deployments and deployment history across teams and projects, adding the ability to query environments, and ensuring that environment’s configurations are traceable, platform engineers can enact stronger controls and avoid costly mistakes in deployments.
              details: |
                * In GitLab, environments are more loosely coupled from the underlying infrastructure when compared to Harness. [When adding an environment](https://docs.gitlab.com/ee/ci/environments/#create-a-static-environment){data-ga-name="link to when adding an environment" data-ga-location="body"}, you don’t need to add its underlying infrastructure at the same time.
                * You can configure [manual deployments](https://docs.gitlab.com/ee/ci/environments/#configure-manual-deployments){data-ga-name="link to manual deployments" data-ga-location="body"} for an environment.
                * GitLab provides a [project-level environment dashboard](https://docs.gitlab.com/ee/ci/environments/#view-environments-and-deployments){data-ga-name="link to project-level environment dashboard" data-ga-location="body"}.
                * You can create [static and dynamic environments](https://docs.gitlab.com/ee/ci/environments/#types-of-environments){data-ga-name="link to static and dynamic environments" data-ga-location="body"}.
                * You can set the [dynamic environment URL](https://docs.gitlab.com/ee/ci/environments/#set-dynamic-environment-urls-after-a-job-finishes){data-ga-name="link to dynamic environment URL" data-ga-location="body"}.
                * You can configure your environments to [track newly included merge requests](https://docs.gitlab.com/ee/ci/environments/#track-newly-included-merge-requests-per-deployment){data-ga-name="link to track newly included merge requests" data-ga-location="body"} per deployment
                * You can perform an [environment rollback](https://docs.gitlab.com/ee/ci/environments/#environment-rollback){data-ga-name="link to environment rollback" data-ga-location="body"}.
                * You can [stop an environment in many different ways](https://docs.gitlab.com/ee/ci/environments/#stop-an-environment){data-ga-name="link to stop an environment in many different ways" data-ga-location="body"} (e.g. when branch is deleted, when MR merged or closed, when another job finishes, after a certain time period, multiple parallel stop actions).
                * You can [delete a stopped and running](https://docs.gitlab.com/ee/ci/environments/#delete-a-stopped-environment){data-ga-name="link to delete a stopped and running environment" data-ga-location="body"} environment.
                * You can [access an environment for preparation or verification](https://docs.gitlab.com/ee/ci/environments/#access-an-environment-for-preparation-or-verification-purposes){data-ga-name="link to access an environment for preparation or verification" data-ga-location="body"} purposes.
                * You can [group similar](https://docs.gitlab.com/ee/ci/environments/#group-similar-environments){data-ga-name="link to group similar environments" data-ga-location="body"} environments.
                * You can [relate an incident to an environment](https://docs.gitlab.com/ee/ci/environments/#environment-incident-management){data-ga-name="link to relate an incident to an environment" data-ga-location="body"}.
                * [GitLab Auto Rollback](https://docs.gitlab.com/ee/ci/environments/#auto-rollback){data-ga-name="link to gitlab auto rollback" data-ga-location="body"} eases this workflow by automatically triggering a rollback when a [critical alert](https://docs.gitlab.com/ee/operations/incident_management/alerts.html){data-ga-name="link to critical alert" data-ga-location="body"} is detected.
                * You can [monitor environments](https://docs.gitlab.com/ee/ci/environments/#monitor-environments){data-ga-name="link to monitor environments" data-ga-location="body"}.
                * [Variables can be scoped](https://docs.gitlab.com/ee/ci/environments/#scope-environments-with-specs){data-ga-name="link to variables can be scoped" data-ga-location="body"} to specific environments.
                * GitLab provides a built-in cross-project environments [dashboard](https://docs.gitlab.com/ee/ci/environments/environments_dashboard.html){data-ga-name="link to built-in cross-project environments dashboard" data-ga-location="body"} as well as project-level environments [dashboard](https://docs.gitlab.com/ee/ci/environments/#view-environments-and-deployments){data-ga-name="link to project-level environments dashboard" data-ga-location="body"}.
                * GitLab allows you to [protect](https://docs.gitlab.com/ee/ci/environments/protected_environments.html#protecting-environments){data-ga-name="link to protect environments" data-ga-location="body"} your environments by determining who is allowed to deploy
                * In addition, GitLab allows you to require [additional approvals](https://docs.gitlab.com/ee/ci/environments/deployment_approvals.html){data-ga-name="link to require additional approvals" data-ga-location="body"} before deploying to certain protected environments.
              improving_product_capabilities: |
                * GitLab Environment Management direction [page](https://about.gitlab.com/direction/release/environment_management/#whats-next){data-ga-name="link to environment management direction page" data-ga-location="body"}
              link_to_documentation: https://docs.gitlab.com/ee/ci/environments/
            competitor:
              coverage: 25
              description: Harness CD environment management capabilities are similar to GitLab. Although managing environments is more manual in Harness, they provide more variety in built-in and custom dashboards for environments.
              details: |
                * In Harness, environments are tightly coupled to the underlying infrastructure. When adding an environment, you need to also add its underlying infrastructure.
                * Harness has wizards to [add Environments](https://docs.harness.io/article/n39w05njjv-environment-configuration){data-ga-name="link to add environments" data-ga-location="body"}
                * Using Harness RBAC, you can [restrict deployment access to specific environments](https://docs.harness.io/article/twlzny81xl-restrict-deployment-access-to-specific-environments){data-ga-name="link to restrict deployment access to specific environments" data-ga-location="body"}.
                * You can [define the infrastructure](https://docs.harness.io/article/v3l3wqovbe-infrastructure-definitions){data-ga-name="link to define the infrastructure" data-ga-location="body"} for environments
                * Harness [Deployment dashboards](https://docs.harness.io/article/c3s245o7z8-main-and-services-dashboards){data-ga-name="link to deployment dashboards" data-ga-location="body"} include Environment information similar to GitLab’s Environment windows
              additional: |
                * Using Environment in Harness [video](https://youtu.be/xKbiAWEC8jk){data-ga-name="link to using environment in harness video" data-ga-location="body"}
              link_to_documentation: https://docs.harness.io/article/n39w05njjv-environment-configuration#next_steps
        overview_analysis: |
          GitLab differentiates from Harness with the ability to set up all of the steps to launch a pipeline or merge. The Harness CD offering is weaker overall since it doesn’t include its own git solution, which forces its customers to either schedule them or set up triggers on the Harness CD side and webhooks on the Git side so that Harness can respond to Git events and invoke pipelines. Additionally, Harness does not offer any Release Orchestration and Evidence capabilities, and relies on integrating to third-parties to fulfill those features. Harness’ one area of differentiation is its more mature Advanced Deployments offering.
        gitlab_product_roadmap:
          - roadmap_item: '**Independent deployments** - for deployments of individual projects that can be deployed in an automated fashion without coordination, developers deploy using [CI/CD pipelines](https://docs.gitlab.com/ee/ci/yaml/gitlab_ci_yaml.html){data-ga-name="link to ci/cd pipelines" data-ga-location="body"} in many different ways.'
          - roadmap_item: '**Kubernetes deployments** - for deployments to Kubernetes, we have support for pull-based GitOps via the [Kubernetes Agent](https://docs.gitlab.com/ee/user/clusters/agent/){data-ga-name="link to kubernetes agent" data-ga-location="body"}. The [CI/CD tunnel](https://about.gitlab.com/blog/2022/01/07/gitops-with-gitlab-using-ci-cd/){data-ga-name="link to ci/cd tunnel" data-ga-location="body"} can also enable a cluster connection to be used from GitLab CI/CD, enabling users to deploy with only minor adjustment to previous setup. '
          - roadmap_item: '**Orchestrated deployment** - for complex deployments, particularly those that require [orchestration](https://about.gitlab.com/direction/release/release_orchestration/){data-ga-name="link to release orchestration" data-ga-location="body"} across multiple projects, release managers use [Releases](https://docs.gitlab.com/ee/user/project/releases/){data-ga-name="link to releases" data-ga-location="body"} to gather artifacts. '
    competitor_cards:
      title: "More comparisons"
      cards:
        - name: "Atlassian"
          icon: plan
          stage: Plan
          description: How does GitLab compare to Atlassian in the Plan stage?
          link: /competition/atlassian/
          data_ga_name: link to gitlab vs atlassian
          data_ga_location: body
        - name: "JFrog"
          icon: package-alt-2
          stage: Package
          description: How does GitLab compare to JFrog in the Package stage?
          link: /competition/jfrog/
          data_ga_name: link to gitlab vs jfrog
          data_ga_location: body
        - name: "Snyk"
          icon: secure-alt-2
          stage: Secure
          description: How does GitLab compare to Snyk in the Secure stage?
          link: /competition/snyk/
          data_ga_name: link to gitlab vs snyk
          data_ga_location: body
