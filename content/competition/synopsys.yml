---
  data:
    competitor: Synopsys
    gitlab_coverage: 50
    competitor_coverage: 75
    subheading: Synopsys is a software security platform, with a portfolio including Software Composition Analysis (SCA), Static Application Security Testing (SAST), and Dynamic Analysis (DAST)
    comparison_table:
      - stage: Govern
        features:
          - feature: Vulnerability Management
            gitlab:
              coverage: 50
              description: GitLab’s vulnerability management solution allows for collaboration between the security team and developers. It provides everything needed to triage and manage vulnerabilities.
              details: |
                * Vulnerability Reports:
                  * The Vulnerability Report provides information about vulnerabilities from scans of the default branch. It contains cumulative results of all successful jobs, regardless of whether the pipeline was successful.
                  * The Vulnerability Report can be filtered to narrow focus on only vulnerabilities matching specific criteria.
                  * Some security scanners output the filename and line number of a potential vulnerability. When that information is available, the vulnerability’s details include a link to the relevant file, in the default branch.
                  * From the Vulnerability Report, the status of one or more vulnerabilities can be changed.
                  * The Activity column indicates the number of issues that have been created for the vulnerability. It also indicates if a vulnerability is no longer detected by automated scans or if a scanner has determined a finding is a false positive.
                  * Details of the vulnerabilities listed in the Vulnerability Report can be exported. The export format is CSV (comma separated values). Note that all vulnerabilities are included because filters don’t apply to the export.
                  * When a user evaluates a vulnerability and decides it requires no more action, it can be marked as Dismissed.
                * Pipeline security:
                  * Shows all vulnerability findings contained in the current branch. This includes existing vulnerabilities from the branch’s parent as well as any newly-introduced findings from commits to the branch.
                  * Finding deduplication is applied here across scan job results. When the scan type, location, and one or more of a finding’s identifiers are the same as another’s, duplicates are removed, simplifying the triage process.
                  * Dismiss irrelevant findings or open an Issue to track.
                  * See a summary of findings by analyzer.
                  * Download scan JSON reports and supporting artifacts such as CSV of the URLs and endpoints from a DAST scan.
                * Merge Request security widget
                  * Provides developers with a view of new vulnerability findings resulting from their code changes as well as any existing vulnerabilities their MR resolves.
                  * When scanners are configured in pipelines run on every commit, developers get immediate feedback on any increase or decrease in security risk from their latest change.
              improving_product_capabilities: |
                * Move from the current severity-based vulnerability classification system to a [risk-based classification](https://gitlab.com/groups/gitlab-org/-/epics/3307). Organizations want to understand more context around the potential impact of a given vulnerability.
                * [Enable defensible risk management processes](https://gitlab.com/groups/gitlab-org/-/epics/3309) and provide [enhanced security visibility and control](https://gitlab.com/groups/gitlab-org/-/epics/3310).
                  * There may be different branches for Dev, Staging, Prod, etc.
              link_to_documentation: https://docs.gitlab.com/ee/user/application_security/vulnerability_report/
            competitor:
              coverage: 100
              description: "Synopsys provides a variety of tools for vulnerability management:"
              details: |
                * CodeDX - an application vulnerability correlation (AVC) solution that consolidates application security (AppSec) results to provide a single source of truth, prioritize critical work, and centrally manage software risk.
                * Coverity on Polaris - Provides a comprehensive, aggregated view of application security with the ability to examine and manage individual security issues. It Incorporates results from Black Duck, Coverity, Seeker and Managed Services Portal
                * Seeker - An interactive application security testing (IAST) solution that enables development, DevOps, and QA teams to perform security testing concurrently with the automated testing of web applications by detecting [vulnerabilities](https://community.synopsys.com/s/document-item?bundleId=seeker_guide&topicId=topics/g_vulnerability.html&_LANG=enus) at runtime. It provides a Web UI to manage the issuesta-ga-name="link to artifactory query language" data-ga-location="body"} to search artifacts and builds using different metadata fields, such as "license type"
                * Coverity on Polaris
                  * Users can generate various reports in PDF, CSV, and HTML formats depicting the current risk status of a chosen application.
                  * Coverity on Polaris keeps track of issues for projects. Getting to the issue list is easy and can be accomplished in several different ways.
                  * Coverity on Polaris provides powerful and flexible filtering capabilities, making it easy to view the issues that are of interest.
                  * Users  can use filter buttons to winnow a large set of issues and then export the results to PDF or CSV.
                  * Coverity on Polaris provides a detailed view of an individual project issue.
                  * After Coverity on Polaris locates issues in a project, the next step is to figure out what to do with them. Users can assign issues to specific people, or can close them as necessary. This process is known as triage.
                  * When the Dismiss Approvals Workflow is on, a project administrator must approve or deny every request to dismiss an issue.
                  * Can be fully managed via API and Command Line
                * Seeker
                  * The dedicated Vulnerability Details page provides a wealth of information about the latest detection of a vulnerability, including its detailed description, classification by major standards, detection samples, remediation guidance, and access to the related online training.
                  * Triaging a vulnerability means assigning it an owner, status, comment, and severity. Seeker supplies a number of out-of-the-box vulnerability statuses. In addition, administrators can configure custom vulnerability statuses.
                  * To track the handling of a vulnerability, create a ticket in a bug-tracking system. Note: Only supports Jira
                  * In addition to displaying detected vulnerabilities in various contexts, Seeker enables users to export this information in different formats.The available formats are CSV, JSON, XML, and PDF report. The scope of exported information matches the currently selected filters, such as projects, severity, status, classification, and more
                * CodeDX
                  * Code Dx reduces the time spent diagnosing issues by normalizing and correlating results from AppSec scanning tools—static and dynamic, commercial, open source, and manual review—into a single console, so users can manage vulnerabilities more effectively.
                  * Quickly assess findings across AST tools with Code Dx Triage Assistant, which uses machine learning to audit historical security decisions and predict critical issues. Ascertain high-impact fixes based on business risk, and provide remediation guidance down to the line of code.
                  * Understand AppSec effectiveness and track testing and remediation progress across pipelines within Code Dx. Communicate defects to developers directly by leveraging Code Dx’s two-way integration with developer feedback tools to assign tasks to team members.
                  * Get a uniform risk assessment of all software components—custom code, third-party, and open source, as well as interrelated components like APIs, containers, and microservices.  Map specific findings to regulatory standards such as NIST, PCI, HIPAA, DISA, and OWASP Top 10, and generate reports to audit software compliance posture.
                  * Developers no longer need to view disparate reports or log into a variety of systems. Code Dx consolidates all AppSec activities into one place, and integrates with 100+ security and developer tools to provide a central platform for AppSec accountability.
                  * The Rule Set Page is accessed via the [Rule Set Associations](https://community.synopsys.com/s/document-item?bundleId=codedx&topicId=user_guide/Project-Management/analysis-config.html&anchor=rule-set-associations&_LANG=enus) section of a project's [Analysis Configuration dialog](https://community.synopsys.com/s/document-item?bundleId=codedx&topicId=user_guide/Project-Management/analysis-config.html&_LANG=enus). When users access the Rule Set page, they will be able to view and sometimes edit a set of rules that can be used to determine how different types of findings will correlate with each other.
              link_to_documentation: https://www.synopsys.com/software-integrity/code-dx.html
          - feature: Compliance Management
            gitlab:
              coverage: 50
              description: GitLab provides several features for establishing, managing, and adhering to compliance.
              details: |
                * Compliance Pipelines
                  * Ensure projects perform the steps necessary to meet regulatory requirements with a common pipeline definition that will run for all projects which adhere to a given compliance framework.
                  * Create unique compliance frameworks that projects must follow. These will appear as a label next a project to distinguish it from others.
                * Compliance Report
                  * View an aggregated list of merge requests for all projects in a group. Easily identify and act on merge requests that are out of compliance or generate and export a chain of custody report for the group's projects.
                * Enforce merge request approval settings
                  * Enforce project merge request approval settings to ensure proper separation of duties.
                  * Send merge request data to third-party systems for validation before merging.
                  * Help teams using both Jira and GitLab better collaborate and stay in sync by requiring that a Jira issue be linked to each merge request.
                * External status checks
                  * Create a status check that sends merge request data to third-party tools. These status checks ensure that all external systems are ready for a merge request to be merged.
                  * Block merges of merge requests unless all status checks have passed.
              improving_product_capabilities: |
                * Bringing the [credential inventory to GitLab.com](https://gitlab.com/groups/gitlab-org/-/epics/5188) to allow managing the credentials that are in use in different groups and projects. This will help group owners to meet compliance requirements and minimize the chance of any credentials being used inappropriately.
                * We are enhancing [external status checks](https://gitlab.com/groups/gitlab-org/-/epics/3869), which allows users to contact external systems as part of a merge request to either create a record in those systems, get back an approval, or any action that may need to be triggered in those external systems.
              link_to_documentation: https://about.gitlab.com/features/?stage=govern#compliance_management
            competitor:
              coverage: 100
              description: "Synopsys provides several features for establishing, managing, and adhering to compliance within the following tools:"
              details: |
                * Seeker: An interactive application security testing (IAST) solution that enables development, DevOps, and QA teams to perform security testing concurrently with the automated testing of web applications by detecting vulnerabilities at runtime.
                  * A compliance policy is a user-defined collection of rules that are applied to a project. The rules define various project metrics such as a timeframe to fix vulnerabilities, or a minimal testing coverage. The results of a project's compliance with its assigned policy are indicated in its banner in the Projects page and in generated reports. 
                  * In the Seeker Compliance dashboard, view the details of a project's compliance with the assigned policy and the major security standards: OWASP Top 10 2021, OWASP Top 10 2017, OWASP Top 10 2013, PCI-DSS v3.2, CWE/SANS 2011, GDPR, and CAPEC.
              link_to_documentation: https://community.synopsys.com/s/document-item?bundleId=seeker_guide&topicId=topics/t_config_compl_pol.html&_LANG=enus
          - feature: Audit Events
            gitlab:
              coverage: 50
              description: Audit Events is a tool for GitLab owners and administrators to track important events such as who performed certain actions and the time they happened. For example, these actions could be a change to a user permission level, who added a new user, or who removed a user. GitLab can help owners and administrators respond to auditors by generating comprehensive reports. These audit reports vary in scope, depending on the needs.
              details: |
                * GitLab offers [around 126 audit events](https://docs.gitlab.com/ee/administration/audit_events.html) with additional events being added in most releases. Key events include user-related events, git operations and security-related events.
                * GitLab offers a way to view the changes made within the GitLab server for owners and administrators on a paid plan.
                * GitLab system administrators can also view all audit events by accessing the audit_json.log file. The JSON audit log does not include events that are only streamed.
                * Filter audit events in the UI to match specific users, projects, groups, or date ranges.
                * Add multiple streaming locations for audit events.
                * Filter streaming audit events to send a subset of events to different streaming locations.
                * Verify streaming audit event veracity with a unique verification token.
                * View sign-in events in the Authentication log.
                * Users can:
                  * View the audit events in the UI and generate a CSV audit event report.
                  * Stream audit events to an external endpoint.
              improving_product_capabilities: |
                * Feature parity between the application's audit events tables and the audit events API and a [comprehensive webhook system](https://gitlab.com/groups/gitlab-org/-/epics/5925).
                * Add more events to [audit](https://gitlab.com/groups/gitlab-org/-/epics/736)
              link_to_documentation: https://docs.gitlab.com/ee/administration/audit_events.html
            competitor:
              coverage: 50
              description: "Synopsys captures and logs certain user-initiated events with potential security impact, such as user login, user management, project creation, and more with the following tools:"
              details: |
                * Coverity on Polaris: Provides a comprehensive, aggregated view of application security with the ability to examine and manage individual security issues. Incorporates results from Black Duck, Coverity, Seeker, and Managed Services Portal
                * CodeDX: An application vulnerability correlation (AVC) solution that consolidates application security (AppSec) results to provide a single source of truth, prioritize critical work, and centrally manage software risk.
                * Seeker: An interactive application security testing (IAST) solution that enables development, DevOps, and QA teams to perform security testing concurrently with the automated testing of web applications by detecting [vulnerabilities](https://community.synopsys.com/s/document-item?bundleId=seeker_guide&topicId=topics/g_vulnerability.html&_LANG=enus) at runtime. It provides a Web UI to manage the issues, It however lacks audit information on what is particularly happening in the SCM which developers are interacting with on a regular basis.
                * Seeker automatically captures and logs certain user-initiated events with potential security impact, such as user login, user management, project creation, and more. Every event entry contains a service name, time stamp, event type and subtype, author information, and other event-specific attributes.
                * Polaris: An organization Administrator can review audit logs that record significant actions that took place during the previous 30 days, whether made through the UI or the API. Audit logs record the following kinds of events:
                  * User login
                  * Changes affecting SAML configurations
                  * Creation or deletion of a service account
                  * Changes that affect an organization admin
                  * Changes in role assignment
                * CodeDX: The Visual Log Page provides a helpful UI for certain events and errors that administrators might be interested in, for auditing purposes. It is important to note that the log file generated by a running Code Dx installation is not the same as the visual log. Most notably, arbitrary exceptions that appear in the log file will typically not appear on the Visual Log Page. This document will provide details about what does appear in the visual log. By default, the Log Filter section will contain a single blank filter block. Each filter block contains three optional criteria:
                  * View only those log entries whose User is one of the users selected in the filter block.
                  * View only those log entries whose Project is one of the projects selected in the filter block.
                  * View only those log entries whose Type is one of the types selected in the filter block.
              link_to_documentation: https://community.synopsys.com/s/document-item?bundleId=seeker_guide&topicId=topics/r_audit_logs.html&_LANG=enus
          - feature: Security Policy Management
            gitlab:
              coverage: 75
              description: GitLab offers several types of policies which can block insecure code from making it into production as well as require scanners to run regardless of the YAML definition every pipeline or on a schedule.
              details: |
                * Security policies
                  * Policies are flexible and can be applied both to individual projects and can also be managed centrally to apply uniformly to all projects in a group. Policies can also be managed for individual projects or subgroups of projects to accommodate varying needs.
                  * Policies can require security scans to run as part of a pipeline or on a set schedule.
                  * Policies can require the approval of a security or compliance team prior to allowing code with vulnerabilities to be merged into production.  Policies are flexible and allow for different types of scanners to have different vulnerability thresholds that require the approval.
                  * Policies can consider both GitLab-native security scanners as well as results from other external scanners.
                  * Security and compliance can enforce scan result policies in a way that developers are not able to override or change the policy.
                  * Users can require any changes to a security policy to go through a two-step approval process where approval from a second person is required before changes are made.
                  * Policies are stored as code, making it easy to copy and replicate security policies across projects.  Additionally an easy-to-use visual policy editor is available, making it easy for non-technical users to manage the policies.
                * License approvals
                  * In addition to enforcing security scans and security approvals, users can also require a merge request to be approved when it contains prohibited licenses.  This allows compliance teams to be confident that unauthorized licenses are not being used in their projects.
                * External checks
                  * For all use cases not covered above, external status checks can be used to call out to a custom API. This allows users almost infinite flexibility in scripting what kind of checks they would like to perform before allowing the status check to succeed.
              link_to_documentation: https://docs.gitlab.com/ee/user/admin_area/credentials_inventory.html
            competitor:
              coverage: 50
              description: "Synopsys provides Intelligent Orchestration provides a complete security orchestration solution "
              details: |
                * Intelligent Orchestration (IO) is a modular, extensible application integrating Synopsys and third party security scanning tools into a Continuous Integration/Continuous Deployment (CI/CD) pipeline. Use IO to orchestrate code scanning based on both specified and expert system criteria. Security tools that IO supports include
                  * Coverity, a static analysis tool
                  * Black Duck, a software composition analysis tool
                  * Seeker, an interactive application security testing tool
                  * Polaris, a collection of services that make it easier to manage application security testing
                  * Sigma, a rapid static analysis tool
                  * Code Dx, an application security automator and dashboard.
                * The IO IQ module is a fully automated, configurable, scanning rules engine that pulls procedural, application, and system information from various sources to make real-time security decisions. It makes the calculations upon which prescriptions are based. IO determines which security scans to run in any given execution of a pipeline by looking at multiple factors, including:
                  * Application risk
                  * Significance of code changes committed
                  * Policy enforcement
                * The Orchestration Engine module abstracts how security tools are integrated and configured to run scans. Users configure application-specific details that IO pipelines use to run tools.
                * The IO Workflow Engine analyzes scan results to determine build viability and converts reports extracted from security tools into a uniform schema. It performs the following functions:
                  * **Bug Tracker**: Automatically creates Jira issue tickets for tracking.
                  * **Build Breaker**: Matches specified break criteria to trigger a build break.
                  * **Notifications**: Provides scan feedback (email, Slack, etc.) and (optionally) feeds various dashboards.
                  * **Out-Of-Band Activity Handler**: Determines what out-of-band activities are triggered.
                  * **Sarif Report**: Creates report from merged tool data (Coverity and Polaris only).
                * IO allows users  to use different risk profiles and policies for application security scanning. Click on Policies in the left sidebar on the IO home page. The Policies page appears. For each type of policy, the organizational default policy appears at the top of their respective section.
                * Define how to manage vulnerabilities when they move through their lifecycle stages in Seeker.
                  * Maintain a collection of statuses that can be assigned to vulnerabilities at their various lifecycle stages. In addition to the out-of-the-box statuses, such as Detected and Fixed, users can define custom statuses. 
              link_to_documentation: https://community.synopsys.com/s/synopsys-intelligent-orchestration
          - feature: Dependency Management
            gitlab:
              coverage: 25
              description:  Creates an SBOM referred to as Dependency List, which combines operating system and application dependencies (including both direct and transitive) dependencies into a single list. Additionally, users can create a CycloneDX formatted SBOM, which is separate from the Dependency List
              details: |
                * Use the dependency list to review a project’s dependencies and key details about those dependencies, including their known vulnerabilities. It is a collection of dependencies in a user’s project, including existing and new findings
                * Contains support for exporting to [CycloneDX](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/#cyclonedx-software-bill-of-materials)
              improving_product_capabilities: |
                * Provide a [group-level searchable view of dependencies](https://gitlab.com/groups/gitlab-org/-/epics/8226) across all projects
                * Better dependency management
                  * [Group/Sub-group dependency list](https://gitlab.com/groups/gitlab-org/-/epics/8090)
                  * [Dependency list filtering and searching](https://gitlab.com/groups/gitlab-org/-/epics/8089)
                  * [Dependency list grouping](https://gitlab.com/groups/gitlab-org/-/epics/8091)
              link_to_documentation: https://docs.gitlab.com/ee/user/application_security/dependency_list/
            competitor:
              coverage: 50
              description: Creates an SBOM using more than just dependencies (includes open-source, 3rd party, and proprietary libraries) and allows exporting in SPDX.
              details: |
                * _Note: Still not completely mature due to the lack of advanced SBOM techniques which can determine which library functions actually impact the system._
                * _Note: **SBOM annotations need to be entered manually**, and while this may seem like a tedious task, it gives substantial benefits to users in the long run, especially if users are scanning new versions of the same software package often. SBOM values can be entered directly from the analysis results, or the values can be added directly to the component itself. When adding the values to the component, the SBOM values are always available for users in future scans. This still being a manual process without logic to automatically create annotations reduces the maturity score._
                * Black Duck’s discovery technology allows users to compile a complete SBOM (Software Bill of Materials) of the open source, third-party, binary packages, and proprietary software components used to build applications and containers.
                * Exporting an SBOM in NTIA-compliant formats such as SPDX and CycloneDX enables users to establish trust in applications and track related security, license, and operational risk.
                * SBOM can be generated as JSON, YAML, RDF and Tag:value
                * Black Duck not only identifies components in an application and builds an SBOM, but it also advises on related areas of security, license, and operation risk, as well as guiding remediation efforts.
                * Provides SBOM annotations.
                * SBOM can be generated using API
              link_to_documentation: https://community.synopsys.com/s/article/Black-Duck-User-Guide-Version-2022-4-0
          - feature: Security Dashboard
            gitlab:
              coverage: 50
              description: Users can use Security Dashboard to view trends about vulnerabilities detected by security scanners
              details: |
                * The project Security Dashboard shows the total number of vulnerabilities over time, with up to 365 days of historical data. Data refreshes daily. It shows statistics for all vulnerabilities.
                * The group Security Dashboard gives an overview of vulnerabilities found in the default branches of projects in a group and its subgroups.
                * Group Security Dashboard shows trends over time in 30, 60, and 90-day lookbacks.
                * Group Security Dashboard also includes a security scorecard where each project is assigned a letter grade according to the highest-severity open vulnerability. Dismissed or resolved vulnerabilities are excluded. Each project can receive only one letter grade and will appear only once in the Project security status report.
                * The Group Security Dashboard charts are also available in each user’s personal Security Center. Charts reflect only the projects the user has added to their Security Center.

              improving_product_capabilities: |
                * Allow to filter vulnerabilities by image name on the [group dashboard](https://gitlab.com/gitlab-org/gitlab/-/issues/217495) as well as [instance dashboard](https://gitlab.com/gitlab-org/gitlab/-/issues/217496)
              link_to_documentation: https://docs.gitlab.com/ee/user/application_security/security_dashboard/
            competitor:
              coverage: 100
              description: "Synopsys provides rich security dashboard with insights to all aspects of applications’ security posture via different tools:"
              details: |
                * Polaris
                  * Coverity on Polaris creates a comprehensive, graphic summary of issues for a project. Issues are the fundamental unit in Coverity on Polaris; Issues belong to a project. The project summary shows a handful of visualizations that help illuminate overall statistics and patterns of issues. Individual branches can also be selected
                  * In Polaris, an application is a collection of projects. The application view shows aggregated results for all the projects that belong to the application.
                * Seeker
                  * The Application Security dashboard provides an overview of the vulnerabilities detected in a regular project, the application's compliance with the security standards, and the results of third-party component analysis.
                  * The Application Security dashboard provides an aggregated overview of the vulnerabilities detected in a composite project (equivalent to GitLab group), which is convenient for assessing the security status of the whole application.
                * CodeDX
                  * The Project Dashboard provides a managerial overview of a project, displaying a set of analytic and trend data which are automatically updated as Code Dx is used. To reach the Project Dashboard page, click the "Dashboard" link on the project from the Project List page.
              link_to_documentation: https://community.synopsys.com/s/document-item?bundleId=codedx&topicId=user_guide/Dashboard/overview.html&_LANG=enus
        overview_analysis: |
          Synopsys excels at providing a comprehensive, highly-recognized, and complete Governance solution. It provides a suite of tools to assess security posture, manage vulnerabilities, and ensure compliance. With that being said, Synopsys tools are complex and make it difficult to get started, documentation is gated, and a lack of transparency may mean requiring the need of professional services.
          
          Synopsys provides many tools which perform similar functions and are highly complex. Each must be integrated together, leading to choice fatigue and confusion.

          GitLab is not as feature-rich as Synopsys in governance, but excels at combining SCM and security within the same platform, and making it easy to get started. While Synopsys requires several tools to be configured for a full DevSecOps solution, which requires significant overhead, GitLab is much easier to setup and configure and requires little to no overhead when implementing governance.
        gitlab_product_roadmap:
          - roadmap_item: "[Auto-resolve](https://gitlab.com/groups/gitlab-org/-/epics/5708) and [auto-dismiss](https://gitlab.com/gitlab-org/gitlab/-/issues/299552) vulnerability policies"
          - roadmap_item: A move from the current severity-based vulnerability classification system to a [risk-based classification](https://gitlab.com/groups/gitlab-org/-/epics/3307).
          - roadmap_item: Providing a [group-level searchable view](https://gitlab.com/groups/gitlab-org/-/epics/8226) of dependencies across all projects.